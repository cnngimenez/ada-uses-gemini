#!/usr/bin/fish

# parameters.fish

# Copyright 2022 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ----------
# Script to test CGI calls and parameters.

printf "20 text/gemini\r\n"

echo "# Uptime"

uptime -p

uptime

echo "# Parameters"
echo "```parameters
AUTH_TYPE=$AUTH_TYPE
REMOTE_USER=$REMOTE_USER
TLS_CLIENT_SUBJECT=$TLS_CLIENT_SUBJECT
TLS_CLIENT_ISSUER=$TLS_CLIENT_ISSUER
TLS_CLIENT_HASH=$TLS_CLIENT_HASH
TLS_CLIENT_NOT_AFTER=$TLS_CLIENT_NOT_AFTER
TLS_CLIENT_NOT_BEFORE=$TLS_CLIENT_NOT_BEFORE
TLS_VERSION=$TLS_VERSION
TLS_CIPHER=$TLS_CIPHER
TLS_CIPHER_STRENGTH=$TLS_CIPHER_STRENGTH

SCRIPT_NAME=$SCRIPT_NAME
PATH_INFO=$PATH_INFO
PATH_TRANSLATED=$PATH_TRANSLATED
DOCUMENT_ROOT=$DOCUMENT_ROOT

REMOTE_ADDR=$REMOTE_ADDR
REMOTE_HOST=$REMOTE_HOST
REMOTE_METHOD=$REMOTE_METHOD

GEMINI_URL=$GEMINI_URL
GEMINI_URL_PATH=$GEMINI_URL_PATH
QUERY_STRING=$QUERY_STRING

GATEWAY_INTERFACE=$GATEWAY_INTERFACE
SERVER_NAME=$SERVER_NAME
SERVER_PROTOCOL=$SERVER_PROTOCOL
SERVER_SOFTWARE=$SERVER_SOFTWARE
```"
