#!/usr/bin/fish

# multitask.fish

# Copyright 2022 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ----------
# Script to test CGI calls and parameters.

printf "20 text/gemini\r\n"

echo "# Multitask testing"
echo "While the CGI script is processing, try to ask for another page!"
echo "You have 10 seconds!"
sleep 10
echo "Sleep 10 seconds ended!"
