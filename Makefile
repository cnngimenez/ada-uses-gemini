# Copyright 2019 Christian Gimenez

# Author: Christian Gimenez   

# Makefile

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEBUGGING_TYPE=optimise

PREFIX=/usr/share/local
BUILD=gprbuild -p -P ada_uses_gemini.gpr
CLEAN=gprclean ada_uses_gemini.gpr

all: static shared binaries server

static:
	$(BUILD) -XLIBRARY_KIND=static

shared:
	$(BUILD) -XLIBRARY_KIND=relocatable

binaries:
	gprbuild -p -P ada_uses_gemini_binaries.gpr

server: server-shared

server-shared:
	gprbuild -p -P agemini_server.gpr -XLINKING_TYPE=shared -XDEBUGGING_TYPE=$(DEBUGGING_TYPE)

server-static:
	gprbuild -p -P agemini_server.gpr -XLINKING_TYPE=static -XDEBUGGING_TYPE=$(DEBUGGING_TYPE) -XLIBRARY_KIND=static

clean:
	$(CLEAN) -XLIBRARY_KIND=static
	$(CLEAN) -XLIBRARY_KIND=relocatable
	gprclean agemini_server.gpr -XLINKING_TYPE=shared
	gprclean agemini_server.gpr -XLINKING_TYPE=static

install: uninstall
	gprinstall -p -P ada_uses_gemini.gpr --prefix=$(PREFIX)

.IGNORE: uninstall
uninstall:
	gprinstall --uninstall --prefix=$(PREFIX) -P ada_uses_gemini.gpr
