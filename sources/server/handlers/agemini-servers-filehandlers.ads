--  agemini-servers-filehandlers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
use Agemini.Requests;
with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Servers.Handlers;
use Agemini.Servers.Handlers;
with Agemini.Servers.Mimetypes;
use Agemini.Servers.Mimetypes;

package Agemini.Servers.Filehandlers is

    Handler_Name : constant Handler_Name_Type := "File Handler     ";

    type File_Handler is new Server_Handler with private;

    procedure Initialise (Handler : in out File_Handler;
                          Root_Path : String;
                          Mime_File : String);
    procedure Initialise (Handler : in out File_Handler;
                          Config : Config_Type);

    function Get_Mime_File (Handler : File_Handler) return String;
    function Get_Mime_File (Handler : File_Handler) return Unbounded_String;
    function Get_Root_Path (Handler : File_Handler) return String;
    function Get_Root_Path (Handler : File_Handler) return Unbounded_String;

    function Is_URL_Valid (Handler : File_Handler; URL : String)
        return Boolean;
    --  Return if the URL points to an existing file in the Server.

    function Get_File_Path (Handler : File_Handler; URL : String)
        return String;
    --  Return the real file path processed from the give URL.
    --
    --  Remove the hostname and use the URL path to construct the file Path.
    --  No validations are Performed.

    function Get_Mime_Type (Handler : File_Handler; Filepath : String)
        return String;
    --  From the file extension, return its MIME type.
    --
    --  Use the file Mime_File specified in the handler to retrieve the MIME
    --  from the File path.

    overriding procedure Handle_Response (Handler : File_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean);
    --  Construct a Gemini Response_Type Instance.
    --
    --  Acording to the given URL, retrieve the file data and MIME type and
    --  create a Response_Type Instance.
    --  If the file does not exists, fill the Response_Type with Not_Found
    --  status.

private

    type File_Handler is new Server_Handler with
    record
        Root_Path : Unbounded_String;
        Mime_File : Unbounded_String;
        Mimes : Mime_Dictionary;
    end record;

    function Get_File_Data (Handler : File_Handler; Filepath : String)
        return String;

end Agemini.Servers.Filehandlers;
