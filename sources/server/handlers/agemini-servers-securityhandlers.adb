--  agemini-servers-securityhandlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Directories;
with Agemini.Statuses;
with Agemini.URLs;
with Agemini.URLs.Sanitisers;

package body Agemini.Servers.SecurityHandlers is

    function File_Exists (Handler : Security_Handler; URL : String)
        return Boolean
    is
        use Ada.Directories;

        Filepath : constant String := Handler.Get_File_Path (URL);
    begin
        return Ada.Directories.Exists (Filepath)
            and then Ada.Directories.Kind (Filepath)
                in Ordinary_File |  Directory;
    end File_Exists;

    function Get_File_Path (Handler : Security_Handler; URL : String)
        return String is
    begin
        --  Sanitisation is not Required.
        --  The Request_Type already sanitised it!
        return Agemini.URLs.Sanitisers.To_Local_Path
            (URL, To_String (Handler.Root_Path));
    end Get_File_Path;

    overriding procedure Handle_Response (Handler : Security_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean)
    is
        URL : constant String := Get_URL (Request);
    begin
        No_Next := False;

        if not Handler.Is_Hostname_Valid (URL) then
            Initialise (Response, Agemini.Statuses.Proxy_Request_Refused,
                        "Hostname is not valid and no proxying to other host"
                        & " or port is processed.");
            No_Next := True;
            return;
        end if;

        if not Handler.File_Exists (URL) then
            Initialise (Response, Agemini.Statuses.Not_Found,
                                  "The file was not Found");
            No_Next := True;
            return;
        end if;
    end Handle_Response;

    procedure Initialise (Handler : in out Security_Handler;
                          Hostname : String;
                          Root_Path : String)
    is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Hostname := To_Unbounded_String (Hostname);
        Handler.Root_Path := To_Unbounded_String (Root_Path);
    end Initialise;

    procedure Initialise (Handler : in out Security_Handler;
                          Config : Config_Type) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Hostname := Config.Get_Hostname;
        Handler.Root_Path := Config.Get_Root_Path;
    end Initialise;

    function Is_Hostname_Valid (Handler : Security_Handler; URL : String)
        return Boolean
    is
        use Agemini.URLs;
    begin
        return Handler.Hostname = Get_Host (URL);
    end Is_Hostname_Valid;

end Agemini.Servers.SecurityHandlers;
