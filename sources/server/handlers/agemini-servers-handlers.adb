--  agemini-servers-handlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body Agemini.Servers.Handlers is

    function Get_Handler_Name (Handler : Server_Handler)
        return Handler_Name_Type
    is (Handler.Handler_Name);

    procedure Handle_Response (Handler : Server_Handler;
                               Request : Request_Type;
                               Response : in out Response_Type;
                               No_Next : out Boolean)
    is
    begin
        No_Next := False;
    end Handle_Response;

    procedure Initialise (Handler : in out Server_Handler;
                          Name : Handler_Name_Type)
    is
    begin
        Handler.Handler_Name := Name;
    end Initialise;

end Agemini.Servers.Handlers;
