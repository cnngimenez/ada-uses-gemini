--  agemini-servers-secure_directoryhandlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Fixed;
with Ada.Characters.Handling;
with Agemini.Statuses;
with Agemini.URLs;

package body Agemini.Servers.Secure_DirectoryHandlers is

    overriding procedure Handle_Response (Handler : Secure_Directory_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean)
    is
        function Prefix (S1 : Unbounded_String; S2 : String) return Boolean;

        function Prefix (S1 : Unbounded_String; S2 : String) return Boolean is
        begin
            return Ada.Strings.Fixed.Head (S2, Length (S1)) = S1;
        end Prefix;

    begin
        No_Next := False;

        if Prefix (Handler.Secure_Path,
                   Agemini.URLs.Get_Path (Request.Get_URL))
        then
            --  Client try to access a secure path... check certificate...

            if not Request.Is_Peer_Certificate_Provided then
                --  No certificate Provided!
                --  Answer with 60: Client certificate required.
                Initialise (Response,
                            Agemini.Statuses.Client_Certificate_Required,
                            "A registered certificate is required.");
                No_Next := True;
                return;
            end if;

            if not Is_Certificate_Authorised (Handler, Request) then
                --  Certificate is not the expected One!
                --  Answer with 61: Certificate not authorised.
                Initialise (Response,
                            Agemini.Statuses.Certificate_Not_Authorised,
                            "Your certificate is not registered.");
                No_Next := True;
                return;
            end if;

            --  Certificate is provided and the expected One.
            --  Just do nothig, the response is allowed.
        end if;
    end Handle_Response;

    procedure Initialise (Handler : in out Secure_Directory_Handler;
                          Secure_Path : String;
                          Valid_Tokens : Token_Vector_Type)
    is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Secure_Path := To_Unbounded_String (Secure_Path);
        Handler.Valid_Tokens := Valid_Tokens;
    end Initialise;

    procedure Initialise (Handler : in out Secure_Directory_Handler;
                          Config : Config_Type) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Secure_Path := Config.Get_Secure_Path;
        Handler.Valid_Tokens := Config.Get_Valid_Tokens;
    end Initialise;

    function Is_Certificate_Authorised (Handler : Secure_Directory_Handler;
                                        Request : Request_Type)
        return Boolean is
        use Ada.Characters.Handling;
    begin
        return Handler.Valid_Tokens.Contains
            (To_Unbounded_String
                (To_Lower (To_String (Request.Get_TLS_Certificate.Hash))));
    end Is_Certificate_Authorised;

end Agemini.Servers.Secure_DirectoryHandlers;
