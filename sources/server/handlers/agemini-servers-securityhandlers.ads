--  agemini-servers-securityhandlers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
use Agemini.Requests;
with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Servers.Handlers;
use Agemini.Servers.Handlers;

--  Provides Security_Handler.  These handlers can allow or reject
--  the usual Response depending on the secure client SSL Connection.
--  If the client possess a registered SSL token, and is requesting a Secure
--  path, then it is Allowed.
--  But if the client do not possess a registered SSL token, and is Requesting
--  a secure path, then it is Rejected.
--  If the client is not requesting a secure path, then nothing is checked.
package Agemini.Servers.SecurityHandlers is

    Handler_Name : constant Handler_Name_Type := "Security Handler ";

    type Security_Handler is new Server_Handler with private;

    procedure Initialise (Handler : in out Security_Handler;
                          Hostname : String;
                          Root_Path : String);
    procedure Initialise (Handler : in out Security_Handler;
                          Config : Config_Type);

    overriding procedure Handle_Response (Handler : Security_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean);

private

    type Security_Handler is new Server_Handler with
    record
        Hostname : Unbounded_String;
        Root_Path : Unbounded_String;
    end record;

    function Get_File_Path (Handler : Security_Handler; URL : String)
        return String;

    function Is_Hostname_Valid (Handler : Security_Handler; URL : String)
        return Boolean;

    function File_Exists (Handler : Security_Handler; URL : String)
        return Boolean;

end Agemini.Servers.SecurityHandlers;
