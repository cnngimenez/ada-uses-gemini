--  agemini-servers-directoryhandlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Characters.Latin_1;
with Ada.Characters;
with Ada.Directories;
with Agemini.URLs;
with Agemini.URLs.Sanitisers;
with Agemini.Statuses;

package body Agemini.Servers.Directoryhandlers is

    function Get_File_Path (Handler : Directory_Handler; URL : String)
        return String is
    begin
        return Agemini.URLs.Sanitisers.To_Local_Path
            (URL, To_String (Handler.Root_Path));
    end Get_File_Path;

    overriding procedure Handle_Response (Handler : Directory_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean)
    is
        URL : constant String := Request.Get_URL;
    begin
        No_Next := False;

        if not Should_Answer (Handler, Request, Response) then
            return;
        end if;

        if URL (URL'Last) = '/' then
            Initialise (Response, Agemini.Statuses.Success,
                        "text/gemini",
                        Make_Index_Gmi (Handler, URL));
        else
            --  The URL is not good, redirect to the directory.
            Initialise (Response,
                        Agemini.Statuses.Redirect_Permanent,
                        URL & "/");
        end if;
    end Handle_Response;

    procedure Initialise (Handler : in out Directory_Handler;
                          Root_Path : String;
                          Listing_Enabled : Boolean := False) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Root_Path := To_Unbounded_String (Root_Path);
        Handler.Listing_Enabled := Listing_Enabled;
    end Initialise;

    procedure Initialise (Handler : in out Directory_Handler;
                          Config : Config_Type) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Root_Path := Config.Get_Root_Path;
        Handler.Listing_Enabled := Config.Get_Listing_Enabled;
    end Initialise;

    function Make_Index_Gmi (Handler : Directory_Handler; URL : String)
        return String
    is
        use Ada.Directories;
        use Ada.Characters;

        Local_Path : constant String := Handler.Get_File_Path (URL);
        --  Do not show the local path in the output Text!
        --  It may expose servers directories (i.e. when using full path!)
        Path : constant String := Agemini.URLs.Get_Path (URL);
        --  This is the URL path only. Use it freely.
        Gmi_String : Unbounded_String;
        --  The Gemtext string builded to send as Output.

        procedure Process_File (Directory_Entry : Directory_Entry_Type);

        procedure Process_File (Directory_Entry : Directory_Entry_Type) is
        begin
            if Simple_Name (Directory_Entry) = "."
                or else Simple_Name (Directory_Entry) = ".."
            then
                return;
            end if;
            Append (Gmi_String, "=> "
                & Simple_Name (Directory_Entry)
                & " "
                & Simple_Name (Directory_Entry)
                & " "
                & File_Kind'Image (Kind (Directory_Entry))
                & " ("
                & File_Size'Image (Size (Directory_Entry))
                & "B)" & Latin_1.LF);
        end Process_File;

    begin
        Append (Gmi_String, "# " & Path  & " Listing" & Latin_1.LF
            & "Files: " & Latin_1.LF);
        Append (Gmi_String,
                "=> . . DIRECTORY ( 0B)" & Latin_1.LF
                & "=> .. .. DIRECTORY ( 0B)" & Latin_1.LF);
        Search (Local_Path, "*",
            Process => Process_File'Access);

        return To_String (Gmi_String);
    end Make_Index_Gmi;

    function Should_Answer (Handler : Directory_Handler;
                            Request : Request_Type;
                            Last_Response : Response_Type)
                            return Boolean
    is
        use Ada.Directories;
        use type Agemini.Statuses.Status_Type;

        URL : constant String := Request.Get_URL;
        Filepath : constant String := Handler.Get_File_Path (URL);
    begin
        return Handler.Listing_Enabled
            and then Get_Status (Last_Response) = Agemini.Statuses.Not_Found
            and then Exists (Filepath)
            and then Kind (Filepath) = Directory;
    end Should_Answer;

end Agemini.Servers.Directoryhandlers;
