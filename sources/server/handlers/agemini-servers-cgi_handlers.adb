--  agemini-servers-cgi_handlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with GNAT.OS_Lib;
with GNAT.Expect;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Directories;
with Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Characters.Latin_1;
with Ada.Exceptions;
use Ada.Exceptions;
with Ada.Strings.Fixed;

with Agemini.URLs.Sanitisers;
with Agemini.URLs;

with TLS.Contexts;

package body Agemini.Servers.CGI_Handlers is

    function Get_File_Path (Handler : CGI_Handler; URL : String)
        return String is
    begin
        --  Sanitisation is not Required.
        --  The Request_Type already sanitised it!
        return Agemini.URLs.Sanitisers.To_Local_Path
            (URL, To_String (Handler.Root_Path), "index.gmi");
    end Get_File_Path;

    overriding procedure Handle_Response (Handler : CGI_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean)
    is
        URL : constant String := Request.Get_URL;
        File_Path : constant String := Handler.Get_File_Path (URL);
        Meta_String, Output_String : Unbounded_String;
        Status : Status_Type;
        Success : Boolean;
    begin
        No_Next := False;

        --  Ada.Text_IO.Put_Line ("CGI_Handler: handling Response.");
        --  Ada.Text_IO.Put_Line
        --    ("CGI_Handler: Looking for file " & File_Path);

        if not Handler.Enabled then
            return;
        end if;

        if not Handler.Is_In_CGI_Directory (File_Path) then
            return;
        end if;

        if not Handler.Is_URL_Valid (URL) then
            --  Ada.Text_IO.Put_Line ("URL is not Valid");
            Initialise (Response, Agemini.Statuses.Not_Found,
                        "The file was not Found");
            return;
        end if;

        Run_CGI_Program (Handler, File_Path, Request,
            Status, Meta_String, Output_String, Success);

        if Success then
            Initialise (Response, Status,
                                  To_String (Meta_String),
                                  To_String (Output_String));
        else
            Initialise (Response, Agemini.Statuses.Cgi_Error,
                        "Error on CGI program.");
        end if;

    end Handle_Response;

    procedure Initialise (Handler : in out CGI_Handler;
                          Root_Path : String;
                          Hostname : String;
                          CGI_Directory : String;
                          Enabled : Boolean := True) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Hostname := To_Unbounded_String (Hostname);
        Handler.Root_Path := To_Unbounded_String (Root_Path);
        Handler.CGI_Directory := To_Unbounded_String (CGI_Directory);
        Handler.Enabled := Enabled;
    end Initialise;

    procedure Initialise (Handler : in out CGI_Handler;
                          Config : Config_Type) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.CGI_Directory := Config.Get_CGI_Directory;
        Handler.Enabled := Config.Get_CGI_Enabled;
        Handler.Hostname := Config.Get_Hostname;
        Handler.Root_Path := Config.Get_Root_Path;
    end Initialise;

    function Is_In_CGI_Directory (Handler : CGI_Handler; Filepath : String)
        return Boolean
    is
        use Ada.Directories;
    begin
        return Base_Name (Containing_Directory (Filepath)) =
            To_String (Handler.CGI_Directory);
    end Is_In_CGI_Directory;

    function Is_URL_Valid (Handler : CGI_Handler; URL : String)
        return Boolean
    is
        use Ada.Directories;

        Filepath : constant String := Handler.Get_File_Path (URL);
    begin
        return Exists (Filepath)
            and then Kind (Filepath) = Ordinary_File;
    end Is_URL_Valid;

    procedure Parse_Output_String (Process_Output : String;
                                   Meta_Line : out Unbounded_String;
                                   Output : out Unbounded_String)
    is
        use Ada.Characters;
        use Ada.Strings.Fixed;

        EOL_Index : Natural;
    begin
        Output := To_Unbounded_String ("");

        EOL_Index := Index (Process_Output, "" & Latin_1.LF);

        if EOL_Index = 0 then
            Meta_Line := To_Unbounded_String
                ("45 CGI Error: no meta line provided by CGI."
                 & Latin_1.CR & Latin_1.LF);
            return;
        end if;

        Meta_Line := To_Unbounded_String
            (Process_Output (Process_Output'First .. EOL_Index));

        --  Put_Line ("Meta string: """
        --  & Process_Output (Process_Output'First .. EOL_Index) & """");

        Output := To_Unbounded_String
            (Process_Output (EOL_Index + 1 .. Process_Output'Last));

        --  Put_Line ("Output string: """
        --  &  Process_Output (EOL_Index + 1 .. Process_Output'Last)  & """");

        exception
        when others =>
            Put_Line ("CGI_Handler: "
                & "Unknown error while parsing CGI output string.");
    end Parse_Output_String;

    procedure Run_CGI_Program (Handler : CGI_Handler;
                               Path : String;
                               Request : Request_Type;
                               Status : out Status_Type;
                               Meta_String : out Unbounded_String;
                               Output : out Unbounded_String;
                               Success : out Boolean)
    is
        use GNAT.OS_Lib;

        procedure Parse_Meta_String;
        --  Separate the Status number and meta string from the first Line.

        procedure Parse_Meta_String is
            Status_End : Natural;
            Status_Int : Natural;
            Status_String : Unbounded_String;
        begin
            Status_End := Index (Meta_String, " ");
            Status_String := Unbounded_Slice (Meta_String, 1, Status_End - 1);
            Status_Int := Natural'Value (To_String (Status_String));
            Status := Status_Type'Enum_Val (Status_Int);

            Meta_String := Unbounded_Slice (Meta_String,
                Status_End + 1, Length (Meta_String));

            exception
            when Constraint_Error =>
                Put_Line ("CGI_Handler: "
                    & "Error when parsing meta line from CGI script.");
                Status := Cgi_Error;
                Meta_String := To_Unbounded_String
                    ("Error parsing meta line from CGI script.");
                Success := False;
                Output := To_Unbounded_String ("");
        end Parse_Meta_String;

        --  Args : constant Argument_List_Access :=
        --      Argument_String_To_List
        --          ("HELLO=WORLD WORLD=HELLO TEST1=T1 TEST2=t2 " & Path);
        Args_String : constant String :=
            Set_Environment_Variables (Handler, Request) & " " & Path;
        Args : Argument_List_Access :=
            Argument_String_To_List (Args_String);
        Return_Code : aliased Integer;
    begin
        Put_Line ("CGI_Handler: Runing process: /usr/bin/env " & Args_String);

        --  Spawn inside a task would not Work!
        --  Non_Blocking_Spawn with Wait_Process mixes the I/O!
        declare
            Process_Output : constant String :=
                GNAT.Expect.Get_Command_Output (
                    Command => "/usr/bin/env",
                    Arguments => Args.all,
                    Input => "",
                    Status => Return_Code'Access);
        begin
            Success := Return_Code = 0;
            if Success then
                Parse_Output_String (Process_Output, Meta_String, Output);
                Parse_Meta_String;
            end if;
        end;

        GNAT.OS_Lib.Free (Args);

        exception
        when E : others =>
            Put_Line ("CGI_Handler: Error when spawning the CGI program:");
            Put_Line (Exception_Information (E));
    end Run_CGI_Program;

    function Set_Environment_Variables (Handler : CGI_Handler;
                                        Request : Request_Type)
                                        return String
    is
        use Agemini.URLs;

        function Certificate_Information return String;
        function CGI_Script_Information return String;
        function Client_Information return String;
        function Request_Information return String;
        function Server_Information return String;

        function Certificate_Information return String is
            use Ada.Calendar.Formatting;

            Connection : constant TLS.Contexts.Connection_Info :=
                Request.Get_TLS_Connection;
            Certificate : constant TLS.Contexts.Certificate_Info :=
                Request.Get_TLS_Certificate;
            TLS_Issuer : constant String := To_String (Certificate.Issuer);
            TLS_Subject : constant String := To_String (Certificate.Subject);
        begin
            return " AUTH_TYPE=CERTIFICATE"
                & " REMOTE_USER=""" & TLS_Issuer & """"
                & " TLS_CLIENT_SUBJECT=""" & TLS_Subject & """"
                & " TLS_CLIENT_ISSUER=""" & TLS_Issuer & """"
                & " TLS_CLIENT_HASH=""" & To_String (Certificate.Hash) & """"
                & " TLS_CLIENT_NOT_AFTER="""
                & Image (Certificate.Not_After) & """"
                & " TLS_CLIENT_NOT_BEFORE="""
                & Image (Certificate.Not_Before) & """"
                --  More about the Connection
                & " TLS_VERSION=""" & To_String (Connection.TLS_Version) & """"
                & " TLS_CIPHER=""" & To_String (Connection.Cipher) & """";
                --  & " TLS_CIPHER_STRENGTH="""
                --  & Connection.Cipher_Strength""Image & """";
        end Certificate_Information;

        function CGI_Script_Information return String is
            Url : constant String := Request.Get_URL;
        begin
            return " SCRIPT_NAME=""" & Handler.Get_File_Path (Url) & """";
            --  & " PATH_INFO="
            --  & " PATH_TRANSLATED="
            --  & " DOCUMENT_ROOT="""
        end CGI_Script_Information;

        function Client_Information return String is
        begin
            return " REMOTE_ADDR=""" & Request.Get_Host & """";
                --  & " REMOTE_HOST=""" &
                --  & " REMOTE_METHOD=""" &
        end Client_Information;

        function Request_Information return String is
            Url : constant String := Request.Get_URL;
        begin
            return " GEMINI_URL=""" & Url & """"
                & " GEMINI_URL_PATH=""" & Get_Path (Url) & """"
                & " QUERY_STRING=""" & Get_Queries (Url) & """";
        end Request_Information;

        function Server_Information return String is
        begin
            return " GATEWAY_INTERFACE=""CGI/0"""
             & " SERVER_NAME=""" & To_String (Handler.Hostname) & """"
             & " SERVER_PROTOCOL=GEMINI"
             & " SERVER_SOFTWARE=""Agemini/0""";
        end Server_Information;

    begin
        return "-i -"
        --
        & Server_Information
        --
        & Client_Information
        --
        & Request_Information
        --
        & CGI_Script_Information
        --
        & (if Request.Is_Peer_Certificate_Provided then
               Certificate_Information
           else
               " AUTH_TYPE=""""");
    end Set_Environment_Variables;

end Agemini.Servers.CGI_Handlers;
