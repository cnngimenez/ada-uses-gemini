--  agemini-servers-cgi_handlers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
use Agemini.Requests;
with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Servers.Handlers;
use Agemini.Servers.Handlers;
with Agemini.Statuses;
use Agemini.Statuses;

--  CGI_Handlers package
--
--  Execute CGI scripts provided if the configuration is enabled and the
--  consulted path is in the CGI_Directory.
package Agemini.Servers.CGI_Handlers is

    Handler_Name : constant Handler_Name_Type := "CGI Handler      ";

    type CGI_Handler is new Server_Handler with private;

    procedure Initialise (Handler : in out CGI_Handler;
                          Root_Path : String;
                          Hostname : String;
                          CGI_Directory : String;
                          Enabled : Boolean := True);
    procedure Initialise (Handler : in out CGI_Handler;
                          Config : Config_Type);

    overriding procedure Handle_Response (Handler : CGI_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean);

private

    type CGI_Handler is new Server_Handler with
    record
        Hostname : Unbounded_String;
        Root_Path : Unbounded_String;
        CGI_Directory : Unbounded_String;
        Enabled : Boolean;
    end record;

    function Get_File_Path (Handler : CGI_Handler; URL : String)
        return String;

    function Is_In_CGI_Directory (Handler : CGI_Handler; Filepath : String)
        return Boolean;

    function Is_URL_Valid (Handler : CGI_Handler; URL : String)
        return Boolean;

    procedure Run_CGI_Program (Handler : CGI_Handler;
                               Path : String;
                               Request : Request_Type;
                               Status : out Status_Type;
                               Meta_String : out Unbounded_String;
                               Output : out Unbounded_String;
                               Success : out Boolean);

    procedure Parse_Output_String (Process_Output : String;
                                   Meta_Line : out Unbounded_String;
                                   Output : out Unbounded_String);
    --  Parse the string created by the CGI program in Run_CGI_Program.
    --
    --  The Meta_String parameter is the first line of the output File.
    --  For example: "20 text/Gemini"
    --  The Output parameter is the rest of the output file.

    function Set_Environment_Variables (Handler : CGI_Handler;
                                        Request : Request_Type)
                                        return String;

end Agemini.Servers.CGI_Handlers;
