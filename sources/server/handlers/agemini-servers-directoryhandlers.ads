--  agemini-servers-directoryhandlers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Agemini.Servers.Handlers;
use Agemini.Servers.Handlers;
with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Requests;
use Agemini.Requests;
with Agemini.Responses;
use Agemini.Responses;

package Agemini.Servers.Directoryhandlers is
    Handler_Name : constant Handler_Name_Type := "Directory Handler";

    type Directory_Handler is new Server_Handler with private;

    procedure Initialise (Handler : in out Directory_Handler;
                          Root_Path : String;
                          Listing_Enabled : Boolean := False);
    procedure Initialise (Handler : in out Directory_Handler;
                          Config : Config_Type);

    overriding procedure Handle_Response (Handler : Directory_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean);

    function Should_Answer (Handler : Directory_Handler;
                            Request : Request_Type;
                            Last_Response : Response_Type)
                            return Boolean;

    function Get_File_Path (Handler : Directory_Handler; URL : String)
        return String;

private

    type Directory_Handler is new Server_Handler with
    record
        Root_Path : Unbounded_String;
        Listing_Enabled : Boolean;
    end record;

    function Make_Index_Gmi (Handler : Directory_Handler; URL : String)
        return String;

end Agemini.Servers.Directoryhandlers;
