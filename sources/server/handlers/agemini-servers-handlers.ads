--  agemini-servers-handlers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

with Agemini.Requests;
use Agemini.Requests;
with Agemini.Responses;
use Agemini.Responses;

package Agemini.Servers.Handlers is

    type Handler_Name_Type is new String (1 .. 17);

    type Server_Handler is tagged private;
    type Server_Handler_Access is access all Server_Handler'Class;

    procedure Initialise (Handler : in out Server_Handler;
                          Name : Handler_Name_Type);

    procedure Handle_Response (Handler : Server_Handler;
                               Request : Request_Type;
                               Response : in out Response_Type;
                               No_Next : out Boolean);
    --  Create a new Response.
    --
    --  Create a response from the Handler information, the Request, the
    --  possibly (last handler) Response.
    --
    --  The No_Next output is set to True when the handler suggest to not
    --  continue processing the Response. Useful for handlers that Implements
    --  security measures.

    function Get_Handler_Name (Handler : Server_Handler)
        return Handler_Name_Type;

    type Handler_Answer_Type is tagged record
        Handler_Name : Handler_Name_Type;
        Answer : Unbounded_String;
        No_Next : Boolean;
    end record;

    package Handler_Answers_Pack is new Ada.Containers.Vectors
        (Index_Type => Positive,
         Element_Type => Handler_Answer_Type);
    subtype Handler_Answer_Vector_Type is Handler_Answers_Pack.Vector;

private

    type Server_Handler is tagged record
        Handler_Name : Handler_Name_Type;
    end record;

end Agemini.Servers.Handlers;
