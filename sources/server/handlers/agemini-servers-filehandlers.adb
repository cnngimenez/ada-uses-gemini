--  agemini-servers-filehandlers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Directories;
with Agemini.URLs;
use Agemini.URLs;
with Agemini.URLs.Sanitisers;
with Agemini.Statuses;

package body Agemini.Servers.Filehandlers is

    function Get_File_Data (Handler : File_Handler; Filepath : String)
        return String
    is
        use Ada.Text_IO;
        use Ada.Directories;

        File_Data : Unbounded_String;
        File : File_Type;
        C : Character;
    begin
        if not (Exists (Filepath) and then Kind (Filepath) = Ordinary_File)
        then
            return "";
        end if;

        Open (File, In_File, Filepath);
        while not End_Of_File (File) loop
            Get_Immediate (File, C);
            Append (File_Data, C);
        end loop;
        Close (File);

        return To_String (File_Data);
    end Get_File_Data;

    function Get_File_Path (Handler : File_Handler; URL : String)
        return String is
    begin
        --  Sanitisation is not Required.
        --  The Request_Type already sanitised it!
        return Agemini.URLs.Sanitisers.To_Local_Path
            (URL, To_String (Handler.Root_Path), "index.gmi");
    end Get_File_Path;

    function Get_Mime_File (Handler : File_Handler) return String is
        (To_String (Handler.Mime_File));

    function Get_Mime_File (Handler : File_Handler) return Unbounded_String is
        (Handler.Mime_File);

    function Get_Mime_Type (Handler : File_Handler; Filepath : String)
        return String
    is
    begin
        return Handler.Mimes.Mime_From_Extension
            (Ada.Directories.Extension (Filepath), "text/gemini");
    end Get_Mime_Type;

    overriding procedure Handle_Response (Handler : File_Handler;
                                          Request : Request_Type;
                                          Response : in out Response_Type;
                                          No_Next : out Boolean)
    is
        URL : constant String := Request.Get_URL;
        File_Path : constant String := Handler.Get_File_Path (URL);
    begin
        No_Next := False;

        --  Ada.Text_IO.Put_Line ("File_Handler: handling Response.");
        --  Ada.Text_IO.Put_Line
        --    ("File_Handler: Looking for file " & File_Path);

        --  Just check if the file is an ordinary File.
        --  Hostname and existence checks are already done by the
        --  Security_Handler.
        if Handler.Is_URL_Valid (URL) then
            Initialise (Response, Agemini.Statuses.Success,
                                  Handler.Get_Mime_Type (File_Path),
                                  File_Path,
                                  File_Data);
        else
            --  Ada.Text_IO.Put_Line ("URL is not Valid");
            Initialise (Response, Agemini.Statuses.Not_Found,
                                  "The file was not Found");
        end if;
    end Handle_Response;

    function Get_Root_Path (Handler : File_Handler) return String is
        (To_String (Handler.Root_Path));

    function Get_Root_Path (Handler : File_Handler) return Unbounded_String is
        (Handler.Root_Path);

    procedure Initialise (Handler : in out File_Handler;
                          Root_Path : String;
                          Mime_File : String) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Root_Path := To_Unbounded_String (Root_Path);
        Handler.Mime_File := To_Unbounded_String (Mime_File);
        Handler.Mimes.Load_From_File (Mime_File);
    end Initialise;

    procedure Initialise (Handler : in out File_Handler;
                          Config : Config_Type) is
    begin
        Agemini.Servers.Handlers.Initialise
            (Server_Handler (Handler), Handler_Name);

        Handler.Root_Path := Config.Get_Root_Path;
        Handler.Mime_File := Config.Get_Mime_File;
        Handler.Mimes.Load_From_File (To_String (Handler.Mime_File));
    end Initialise;

    function Is_URL_Valid (Handler : File_Handler; URL : String)
        return Boolean
    is
        use Ada.Directories;

        Filepath : constant String := Handler.Get_File_Path (URL);
    begin
        return Exists (Filepath)
            and then Kind (Filepath) = Ordinary_File;
    end Is_URL_Valid;

end Agemini.Servers.Filehandlers;
