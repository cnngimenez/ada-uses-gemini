--  agemini-servers-socketmanagers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with GNAT.Sockets;
use GNAT.Sockets;

with TLS.Contexts;
use TLS.Contexts;
with TLS.Contexts.Server;
use TLS.Contexts.Server;

with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
with Agemini.Servers.Constants;
use Agemini.Servers.Constants;

package Agemini.Servers.Socketmanagers is

    type Sockets_Range is range 1 .. Max_Sockets;
    type Socket_List_Type is
        array (Sockets_Range) of aliased Socket_Type;
    type Context_List_Type is
        array (Sockets_Range) of aliased Server_Context;
    type Free_List_Type is array (Sockets_Range) of Boolean;
    type Server_Context_Access is access all Server_Context;
    type Socket_Access is access all Socket_Type;

    protected Socket_Manager is
        procedure Initialise;

        procedure Register_Busy (Index : Sockets_Range);
        --  Register a set of sockets as being used.

        procedure Close (Index : Sockets_Range);
        --  Close Connection.
        --
        --  This can be used, it would not generate deadlock nor Starvation.

        function Has_Free_Sockets return Boolean;

        function Find_Free_Socket_Index return Natural;
        --  Return the index of a free Socket.
        --
        --  Return 0 if no free socket were Found.

        procedure Get_Sockets (Index : Sockets_Range;
                               Socket : out Socket_Access;
                               TLS_Socket : out Server_Context_Access;
                               Valid : out Boolean);

        function Is_Initialised (Index : Sockets_Range) return Boolean;
        function Is_Configured (Index : Sockets_Range) return Boolean;
        function Is_Connected (Index : Sockets_Range) return Boolean;

        function Image return String;

        --  --------------------------------------------------
        --  Avoid using these subprograms... it may lead to Starvation
        --  --------------------------------------------------

        entry Accept_Connection (Server_TLS : in out Server_Context;
                                 Server_Socket : Socket_Type;
                                 Address : in out Sock_Addr_Type;
                                 Index : out Natural);

        --  Accept a client Connection.
        --
        --  Bettern not to use this to avoid deadlock or starvation: other
        --  subprograms would not execute because it is waiting for new
        --  clients to accept their Connection!
        --
        --  Better:
        --  1. Let a task get a free pair of sockets with
        --  Find_Free_Socket_Index
        --  2. Accept the connection there.
        --  3. Use Register_Busy.

        procedure Receive_Request (Index : Sockets_Range;
                            Request : out Agemini.Requests.Request_Type);
        procedure Send (Index : Sockets_Range;
                        Message : String);
        procedure Send (Index : Sockets_Range;
                        Response : Response_Type);

    private
        procedure Receive_Line (Index : Sockets_Range;
                                Line : out Unbounded_String);

        Connection_Socket_List : Socket_List_Type;
        Connection_TLS_List : Context_List_Type;
        Free_Socket : Free_List_Type;
    end Socket_Manager;

end Agemini.Servers.Socketmanagers;
