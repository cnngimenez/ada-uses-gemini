--  agemini-servers-handlermanagers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;

package body Agemini.Servers.Handlermanagers is

    procedure Add_Handler (Manager : in out Handler_Manager;
                           Handler : Server_Handler_Access) is
    begin
        Manager.Handlers.Append (Handler);
    end Add_Handler;

    --  procedure Initialise (Manager : in out Handler_Manager) is
    --  begin
    --      null;
    --  end Initialise;

    function Get_Handler_Answers (Manager : Handler_Manager)
        return Handler_Answer_Vector_Type
    is (Manager.Handler_Answers);

    function Get_Response (Manager : in out Handler_Manager;
                           Request : Request_Type)
                           return Response_Type
    is
        use Ada.Strings.Unbounded;
        use Handler_Vector_Pack;

        Response : Response_Type;
        Handler_Answer : Handler_Answer_Type;
        No_Next : Boolean := False;
        Position : Handler_Vector_Pack.Cursor;
    begin
        Manager.Handler_Answers.Clear;

        Position := Manager.Handlers.First;
        while not No_Next and then Has_Element (Position) loop
            Element (Position).Handle_Response (Request, Response, No_Next);

            Handler_Answer.Handler_Name := Element (Position).Get_Handler_Name;
            Handler_Answer.Answer :=
                To_Unbounded_String (To_String (Response));
            Handler_Answer.No_Next := No_Next;
            Manager.Handler_Answers.Append (Handler_Answer);

            Next (Position);
        end loop;

        return Response;
    end Get_Response;

    --  Function Get_Local_Path (Manager : Handler_Manger; URL : String)
    --      return Manager_Answers_Type
    --  is
    --      Answers : Manager_Answers_Type;
    --      Temp : Manager_Answer_Type;
    --  begin
    --      for Handler of Manager.Handlers loop
    --          Temp.Manager_Name := Handler.Get_Name;
    --          Temp.Answer := Handler.Get_Local_Path (URL);
    --          Answers.Append (Temp);
    --      end loop;

    --      return Answers;
    --  end Get_Local_Path;
end Agemini.Servers.Handlermanagers;
