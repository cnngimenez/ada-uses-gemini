--  agemini-servers-workers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Agemini.Servers.Handlermanagers;
use Agemini.Servers.Handlermanagers;
with Agemini.Servers.Socketmanagers;
use Agemini.Servers.Socketmanagers;

package Agemini.Servers.Workers is

    task type Client_Worker is
        entry Initialise (Handlers_M : Handler_Manager;
                          Index : Natural);
        entry Start (The_Socket_Index : Sockets_Range);
    end Client_Worker;

    type Client_Worker_Access is access all Client_Worker;

end Agemini.Servers.Workers;
