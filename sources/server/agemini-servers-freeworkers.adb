--  agemini-servers-freeworkers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body Agemini.Servers.Freeworkers is

    protected body Free_Workers is
        function Find_Free_Worker return Natural is
            Found : Boolean := False;
            I : Natural := 1;
        begin
            while I <= Max_Workers and then not Found loop
                Found := Free_Workers.Status_List (Workers_Range (I)) = Free;
                I := I + 1;
            end loop;

            if Found then
                return I - 1;
            else
                return 0;
            end if;
        end Find_Free_Worker;

        procedure Initialise is
        begin
            for I in Status_List'Range loop
                Free_Workers.Status_List (I) := Free;
            end loop;
        end Initialise;

        procedure Mark_Busy (Index : Workers_Range) is
        begin
            Free_Workers.Status_List (Index) := Busy;
        end Mark_Busy;

        procedure Mark_Free (Index : Workers_Range) is
        begin
            Free_Workers.Status_List (Index) := Free;
        end Mark_Free;

    end Free_Workers;

end Agemini.Servers.Freeworkers;
