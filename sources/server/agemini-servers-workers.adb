--  agemini-servers-workers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Exceptions;
use Ada.Exceptions;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Sequential_IO;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Directories;
with Ada.Characters.Latin_1;
with Ada.Characters;
with GNAT.Sockets;
use GNAT.Sockets;
with TLS.Contexts;
use TLS.Contexts;
with TLS.Contexts.Server;
use TLS.Contexts.Server;

--  with System.Address_Image;
with Agemini.Requests;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Servers.Freeworkers;
use Agemini.Servers.Freeworkers;

package body Agemini.Servers.Workers is
    procedure Receive_Line (TLS_Socket : Server_Context_Access;
                            Line : out Unbounded_String);
    procedure Receive_Request (Socket : Socket_Access;
                               TLS_Socket : Server_Context_Access;
                               Request : out Agemini.Requests.Request_Type);
    --  procedure Send (TLS_Socket : Server_Context_Access; Message : String);
    procedure Send (TLS_Socket : Server_Context_Access;
                    Response : Response_Type);
    procedure Send_File (TLS_Socket : Server_Context_Access;
                         Filepath : String);

    procedure Receive_Line (TLS_Socket : Server_Context_Access;
                            Line : out Unbounded_String)
    is
        use Ada.Characters;

        C1, C2 : Character := ' ';
        I : Natural := 0;
    begin
        Line := To_Unbounded_String ("");
        while I <= 1024 and then C1 /= Latin_1.CR and then C2 /= Latin_1.LF
        loop
            C1 := C2;

            Character'Read (TLS_Socket, C2);
            Append (Line, C2);

            I := I + 1;
        end loop;

        if I > 1024 then
            Line := Null_Unbounded_String;
            return;
        end if;

        Delete (Line, Length (Line) - 1, Length (Line));
    end Receive_Line;

    procedure Receive_Request (Socket : Socket_Access;
                               TLS_Socket : Server_Context_Access;
                               Request : out Agemini.Requests.Request_Type)
    is
        use type GNAT.Sockets.Family_Type;
        use type GNAT.Sockets.Family_Inet_4_6;

        Request_String : Unbounded_String;
        Peer_Addr : GNAT.Sockets.Sock_Addr_Type;
    begin
        --  Gemini specification only states that there should be a
        --  request line.
        Receive_Line (TLS_Socket, Request_String);
        Request.Initialise (Request_String);

        --  We add more information to the request type for the Handlers.
        --  The handlers may want to know about the TLS certificate and The
        --  client Host.

        --  Request.Set_TLS_Connection (Get_Connection_Info
        --      (Connection_TLS_List (Index)));
        --  ^^ This one gives failed precondition error tls-context.ads:220
        --  Uncomment this when tlsada has been Fixed.

        if Peer_Certificate_Provided (TLS_Socket.all) then
            --  Put_Line ("Worker: Certificate provided!");
            Request.Set_TLS_Certificate
                (Get_Certificate_Info (TLS_Socket.all));
        end if;

        Peer_Addr := GNAT.Sockets.Get_Peer_Name (Socket.all);
        if Peer_Addr.Family in Family_Inet_4_6 then
            --  I don't think it would be any other Family,
            --  but just in case...
            Request.Set_Host (GNAT.Sockets.Image (Peer_Addr.Addr));
            Request.Set_Port (TLS.Contexts.Port_Number (Peer_Addr.Port));
        end if;

    end Receive_Request;

    --  procedure Send (TLS_Socket : Server_Context_Access; Message : String)
    --  is
    --  begin
    --      --  Put_Line ("Worker: Sending: " & Message);
    --      String'Write (TLS_Socket, Message);
    --  end Send;

    procedure Send (TLS_Socket : Server_Context_Access;
                    Response : Response_Type)
    is
    begin
        --  Put_Line ("Worker: Sending: " & Response.To_String);
        case Response.Body_Content is
        when Text_Data =>
            String'Write (TLS_Socket, To_String (Response));
        when File_Data =>
            String'Write (TLS_Socket, To_String (Response));
            Send_File (TLS_Socket, Get_Body (Response));
        end case;
    end Send;

    procedure Send_File (TLS_Socket : Server_Context_Access;
                         Filepath : String) is
        use Ada.Directories;
        package Cio is new Ada.Sequential_IO (Character);

        --  procedure Fill_Buffer;

        Buffer_Max : constant Positive := 1024;
        --  Number of bytes the buffer can Have.

        Empty_String : constant Unbounded_String := To_Unbounded_String ("");
        Buffer : Unbounded_String;
        File : Cio.File_Type;

        --  procedure Fill_Buffer is
        --      Byte_Count : Natural := 0;
        --      C : Character;
        --  begin
        --      Byte_Count := 0;
        --      while not End_Of_File (File) and then Byte_Count <= Buffer_Max
        --      loop
        --          Get_Immediate (File, C);
        --          Append (Buffer, C);
        --          Byte_Count := Byte_Count + 1;
        --      end loop;
        --  end Fill_Buffer;

        C : Character;
    begin
        if not (Exists (Filepath) and then Kind (Filepath) = Ordinary_File)
        then
            --  We send something just in case...
            String'Write (TLS_Socket, "     ");
            return;
        end if;

        Cio.Open (File, Cio.In_File, Filepath, "shared=no");
        while not Cio.End_Of_File (File) loop
            --  Fill_Buffer;
            --  String'Write (TLS_Socket, To_String (Buffer));
            --  Buffer := Empty_String;
            Cio.Read (File, C);
            Character'Write (TLS_Socket, C);
        end loop;
        Cio.Close (File);

        exception
        when Error : others =>
            Put_Line ("Worker: Error when sending File: "
                & Exception_Message (Error)
                & ". Closing file...");
            Flush;
            Cio.Close (File);
            raise;
    end Send_File;

    task body Client_Worker is
        --  Sockets : Socket_Manager;
        Socket_Index : Sockets_Range;
        TLS_Socket : Server_Context_Access;
        Socket : Socket_Access;
        Valid : Boolean;
        Handlers : Handler_Manager;
        My_Index : Natural := 0;

        procedure Process_Connection;
        procedure Put_Message (Message : String);

        procedure Process_Connection is
            Request : Agemini.Requests.Request_Type;
        begin
            Put_Message ("Receiving request: ");
            Receive_Request (Socket, TLS_Socket, Request);
            Put_Line (Request.Image);

            Send (TLS_Socket, Handlers.Get_Response (Request));
            --  Socket_Manager.all.Send (Socket_Index,
            --      "20 gemini/text"
            --      & ASCII.CR & ASCII.LF & "Hello World!");
            Socket_Manager.Close (Socket_Index);
            Put_Message ("Socket Closed.");
            --  Put_Line ("Worker: Sockets: " & Socket_Manager.Image);

            Free_Workers.Mark_Free (Workers_Range (My_Index));
        exception
            when Error : others =>
                Put_Message ("Error: " & Exception_Message (Error));
                Flush;
                Socket_Manager.Close (Socket_Index);
                Free_Workers.Mark_Free (Workers_Range (My_Index));
        end Process_Connection;

        procedure Put_Message (Message : String) is
        begin
            Put_Line ("Worker " & My_Index'Image & ": " & Message);
        end Put_Message;

    begin
        Put_Message ("Waiting for Initialisation");
        --  Put_Line ("Worker: Socket manager Address: "
        --      & System.Address_Image (Socket_Manager'Address));

        select
            accept Initialise (Handlers_M : Handler_Manager;
                               Index : Natural)
            do
                --  Sockets := Manager;
                Handlers := Handlers_M;
                My_Index := Index;
            end Initialise;
        or
            terminate;
        end select;

        Put_Message ("Initialised with number " & My_Index'Image & ".");

        loop
            select
                accept Start (The_Socket_Index : Sockets_Range) do
                    Socket_Index := The_Socket_Index;
                    Free_Workers.Mark_Busy (Workers_Range (My_Index));

                    Socket_Manager.Get_Sockets (Socket_Index,
                                                Socket, TLS_Socket, Valid);
                end Start;
            or
                terminate;
            end select;

            --  Put_Line ("Worker: Socket manager Address: "
            --      & System.Address_Image (Socket_Manager'Address));

            --  Put_Message ("Sockets: " & Socket_Manager.Image);
            Put_Message ("Processing socket: " & Socket_Index'Image);
            --  Put_Line ("Worker: TLS Initialised: " & Boolean'Image
            --      (Socket_Manager.Is_Initialised (Socket_Index)));
            --  Put_Line ("Worker: TLS Configured: " & Boolean'Image
            --      (Socket_Manager.Is_Configured (Socket_Index)));
            --  Put_Line ("Worker: TLS Connected: " & Boolean'Image
            --      (Socket_Manager.Is_Connected (Socket_Index)));
            if Valid then
                Process_Connection;
            end if;
        end loop;

    exception
        when Error : others =>
            Put_Message ("Error: " & Exception_Message (Error));
            Flush;
    end Client_Worker;
end Agemini.Servers.Workers;
