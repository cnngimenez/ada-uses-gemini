--  agemini-servers-handlermanagers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Containers.Vectors;
with Agemini.Servers.Handlers;
use Agemini.Servers.Handlers;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
use Agemini.Requests;

package Agemini.Servers.Handlermanagers is
    type Handler_Manager is tagged private;

    --  No initialisation is required.  Just use it!
    --  procedure Initialise (Manager : in out Handler_Manager);

    procedure Add_Handler (Manager : in out Handler_Manager;
                           Handler : Server_Handler_Access);

    function Get_Response (Manager : in out Handler_Manager;
                           Request : Request_Type)
                           return Response_Type;

    function Get_Handler_Answers (Manager : Handler_Manager)
        return Handler_Answer_Vector_Type;

    --  function Get_Local_Path (Manager : Handler_Manager; URL : String)
    --      return Manager_Answers_Type;

private

    package Handler_Vector_Pack is new Ada.Containers.Vectors
        (Index_Type => Positive,
         Element_Type => Server_Handler_Access);
    subtype Handler_Vector is Handler_Vector_Pack.Vector;

    type Handler_Manager is tagged record
        Handlers : Handler_Vector;
        Handler_Answers : Handler_Answer_Vector_Type;
    end record;

end Agemini.Servers.Handlermanagers;
