--  agemini-servers-mimetypes.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Strings.Maps;
use Ada.Strings.Maps;
with Ada.Characters.Latin_1;
with Ada.Characters;
use Ada.Characters;

package body Agemini.Servers.Mimetypes is

    procedure Add (Mimes : in out Mime_Dictionary;
                   Extension : String; Mime_Type : String) is
        use String_Hash_Package;
    begin
        if Contains (Mimes.Dictionary, To_Unbounded_String (Extension)) then
            --  Do not add it! this extension is already registered!
            return;
        end if;

        Insert (Mimes.Dictionary,
                To_Unbounded_String (Extension),
                To_Unbounded_String (Mime_Type));
    end Add;

    function Amount (Mimes : Mime_Dictionary) return Natural
        is (Natural (String_Hash_Package.Length (Mimes.Dictionary)));

    procedure Load_From_File (Mimes : in out Mime_Dictionary;
                              File_Path : String)
    is
        procedure Process_Line (Line : String);

        File : File_Type;
        Blank_Charset : constant Character_Set :=
            To_Set (Latin_1.Space & Latin_1.HT);

        procedure Process_Line (Line : String) is
            use Ada.Strings.Fixed;
            First_Blank_Start, Extension_Start, Extension_End : Natural;
        begin
            --  if Is_Comment (Line) then
            --      return;
            --  end if;

            First_Blank_Start := Index (Line, Blank_Charset, Line'First);
            Extension_Start := Index_Non_Blank (Line,  First_Blank_Start);

            if First_Blank_Start = 0 or else Extension_Start = 0 then
                return;
            end if;

            Extension_End := Index (Line, Blank_Charset, Extension_Start);
            if Extension_End = 0 then
                Extension_End := Line'Last;
            end if;

            declare
                Mime_Type : constant String :=
                    Line (Line'First .. First_Blank_Start - 1);
                Extension : constant String :=
                    Line (Extension_Start .. Extension_End);
            begin
                --  Put_Line ("Registering:" & Extension & " -> " & Mime_Type);
                Mimes.Add (Extension, Mime_Type);
            end;

        end Process_Line;

    begin
        Open (File, In_File, File_Path);
        while not End_Of_File (File) loop
            Process_Line (Get_Line (File));
        end loop;
        Close (File);
    end Load_From_File;

    function Mime_From_Extension (Mimes : Mime_Dictionary;
                                  Extension : String;
                                  Default : String := "")
                                  return String
    is
        use String_Hash_Package;
        Key : constant Unbounded_String := To_Unbounded_String (Extension);
    begin
        if not Contains (Mimes.Dictionary, Key) then
            return Default;
        else
            return To_String (Element (Mimes.Dictionary, Key));
        end if;
    end Mime_From_Extension;

end Agemini.Servers.Mimetypes;
