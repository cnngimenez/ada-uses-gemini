--  agemini-servers-workermanagers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Agemini.Servers.Handlermanagers;
use Agemini.Servers.Handlermanagers;
with Agemini.Servers.Workers;
use Agemini.Servers.Workers;
with Agemini.Servers.Constants;
use Agemini.Servers.Constants;

package Agemini.Servers.Workermanagers is

    type Worker_Manager is tagged limited private;

    procedure Initialise (Manager : in out Worker_Manager;
                          Handlers : Handler_Manager);
    procedure Add (Manager : in out Worker_Manager;
                   Socket_Index : Natural);

private

    type Worker_List_Type is array (1 .. Max_Workers) of Client_Worker;

    type Worker_Manager is tagged limited record
        Worker_List : Worker_List_Type;
        --  Sockets : access Socket_Manager;
    end record;

end Agemini.Servers.Workermanagers;
