--  agemini-servers-constants.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package Agemini.Servers.Constants is

    Max_Workers : constant Natural := 10;
    --  Maximum amount of task to use.
    --  TODO: This could be a configuration variable in later Versions.

    --  Max_Sockets : constant Natural := 1; --  Only for debugging!
    Max_Sockets : constant Natural := Max_Workers * 2;
    --  Maximum amount of sockets to use.

end Agemini.Servers.Constants;
