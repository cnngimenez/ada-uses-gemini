--  agemini-servers-mimetypes.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Containers.Hashed_Maps;
with Ada.Strings.Unbounded.Hash;

package Agemini.Servers.Mimetypes is

    type Mime_Dictionary is tagged private;

    --  procedure Initialise (Mime : in out Mime_Dictionary);
    --  No initialization needed!

    procedure Load_From_File (Mimes : in out Mime_Dictionary;
                              File_Path : String);

    procedure Add (Mimes : in out Mime_Dictionary;
                   Extension : String; Mime_Type : String);

    function Mime_From_Extension (Mimes : Mime_Dictionary;
                                  Extension : String;
                                  Default : String := "")
                                  return String;
    --  Return a MIME-type from the file Extension.

    function Amount (Mimes : Mime_Dictionary) return Natural;

private
    package String_Hash_Package is new Ada.Containers.Hashed_Maps
        (Key_Type => Unbounded_String,
         Element_Type => Unbounded_String,
         Hash => Ada.Strings.Unbounded.Hash,
         Equivalent_Keys => "=");
    subtype Mime_Hash_Type is String_Hash_Package.Map;

    type Mime_Dictionary is tagged record
        Dictionary : Mime_Hash_Type;
    end record;

end Agemini.Servers.Mimetypes;
