--  agemini_config_test.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Unbounded;

--  Handlers

--  with Agemini.Servers.Handlers;
--  use Agemini.Servers.Handlers;
with Agemini.Servers.SecurityHandlers;
use Agemini.Servers.SecurityHandlers;
with Agemini.Servers.Filehandlers;
use Agemini.Servers.Filehandlers;
with Agemini.Servers.Directoryhandlers;
use Agemini.Servers.Directoryhandlers;
with Agemini.Servers.Secure_DirectoryHandlers;
use Agemini.Servers.Secure_DirectoryHandlers;
with Agemini.Servers.CGI_Handlers;
use Agemini.Servers.CGI_Handlers;
with Agemini.Servers.Handlermanagers;
use Agemini.Servers.Handlermanagers;

with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Servers.Mimetypes;

with Agemini.URLs;
with Agemini.URLs.Sanitisers;

with Agemini.Responses;
use Agemini.Responses;
with Agemini.Requests;
use Agemini.Requests;

with TLS.Contexts;

procedure Agemini_Config_Test is
    procedure Add_Handlers;
    procedure Print_Config;
    procedure Validate_Config;
    procedure Validate_Mime_File;
    procedure Print_Handler_Responses;
    procedure Print_Url;
    function Parameters_To_Certificate_Info
        return TLS.Contexts.Certificate_Info;

    Server_Config : Config_Type;
    Handlers : Handler_Manager;
    Chandler : aliased CGI_Handler;
    Fhandler : aliased File_Handler;
    Dhandler : aliased Directory_Handler;
    Sdhandler : aliased Secure_Directory_Handler;
    SecHandler : aliased Security_Handler;

    procedure Add_Handlers is
    begin
        SecHandler.Initialise (Server_Config);
        Sdhandler.Initialise (Server_Config);
        Fhandler.Initialise (Server_Config);
        Chandler.Initialise (Server_Config);
        Dhandler.Initialise (Server_Config);

        Handlers.Add_Handler (SecHandler'Unchecked_Access);
        Handlers.Add_Handler (Sdhandler'Unchecked_Access);
        Handlers.Add_Handler (Fhandler'Unchecked_Access);
        Handlers.Add_Handler (Chandler'Unchecked_Access);
        Handlers.Add_Handler (Dhandler'Unchecked_Access);
    end Add_Handlers;

    function Parameters_To_Certificate_Info
        return TLS.Contexts.Certificate_Info is
        use TLS.Contexts;
        use Ada.Strings.Unbounded;

        Info : Certificate_Info;
    begin
        if Argument_Count >= 3 then
            Info.Issuer := To_Unbounded_String (Argument (3));
        end if;
        if Argument_Count >= 4 then
            Info.Hash := To_Unbounded_String (Argument (4));
        end if;
        return Info;
    end Parameters_To_Certificate_Info;

    procedure Print_Config is
    begin
        Put_Line ("Configuration loaded:");
        Put_Line (Image (Server_Config));
    end Print_Config;

    procedure Print_Handler_Responses is
    begin
        Put_Line ("--  Showing each handler responses:");
        for Handler_Answer of Handlers.Get_Handler_Answers loop
            Put_Line ("--------------------------------------------------");
            Put_Line ("  Handler: "
                & String (Handler_Answer.Handler_Name));
            Put_Line ("  No next:" & Handler_Answer.No_Next'Image);
            Put_Line ("  Answer:");
            Put_Line (Ada.Strings.Unbounded.To_String (Handler_Answer.Answer));
        end loop;
    end Print_Handler_Responses;

    procedure Print_Url is
        use Agemini.URLs;

        Original_Url : constant String := Argument (2);
        Url : constant String :=
            Agemini.URLs.Sanitisers.Sanitise (Original_Url);

    begin
        Put_Line ("URL Processing:");
        Put_Line ("URL input: """ & Original_Url & """");
        Put_Line ("URL sanitised: """ & Url & """");
        Put_Line ("Scheme: """ & Get_Scheme (Url) & """");
        Put_Line ("Authority: """ & Get_Authority (Url) & """");
        Put_Line ("Host: """ & Get_Host (Url) & """");
        Put_Line ("Path: """ & Get_Path (Url) & """");
        Put_Line ("File: """ & Get_File (Url) & """");
        Put_Line ("Queries: """ & Get_Queries (Url) & """");
    end Print_Url;

    procedure Validate_Config is
    begin
        if Server_Config.Is_Key_File_Valid then
            Put_Line ("- Key file is found and valid.");
        else
            Put_Line ("- Key file not found or invalid:");
            Put_Line ("  " & Server_Config.Get_Key_File);
        end if;

        if Server_Config.Is_Cert_File_Valid then
            Put_Line ("- Cert file is found and valid.");
        else
            Put_Line ("- Cert file not found or is invalid:");
            Put_Line ("  " & Server_Config.Get_Cert_File);
        end if;

        if Server_Config.Is_Mime_File_Valid then
            Put_Line ("- MIME file is found and valid.");
        else
            Put_Line ("- MIME file is not found or is invalid:");
            Put_Line ("  " & Server_Config.Get_Mime_File);
        end if;

        if Server_Config.Is_Root_Path_Valid then
            Put_Line ("- Root path is valid.");
        else
            Put_Line ("- Root path is not valid:");
            Put_Line ("  " & Server_Config.Get_Root_Path);
        end if;
    end Validate_Config;

    procedure Validate_Mime_File is
        use Agemini.Servers.Mimetypes;

        Mimes : Mime_Dictionary;
        Extension_Tests : constant array (Positive range <>) of
            String (1 .. 3) :=
            ["gmi", "txt",
             "ogg",
             "png", "jpg",
             "pdf"];
    begin
        Put_Line ("Validating MIME file.");
        Mimes.Load_From_File (Server_Config.Get_Mime_File);

        Put_Line ("- MIME types loaded: "
            & Mimes.Amount'Image & " MIME-types Loaded.");

        for Extension of Extension_Tests loop
            Put_Line ("- extension " & Extension & " is MIME: "
                    & Mimes.Mime_From_Extension (Extension));
        end loop;
    end Validate_Mime_File;

    Response : Response_Type;
    Request : Request_Type;
begin
    if Argument_Count = 0 or else Argument (1) = "default" then
        Server_Config.Initialise_Default;
        Put_Line ("Using default configuration.");
        --  Put_Line ("See ./agemini_server -h for more information.");
    else
        Server_Config.Load_File (Argument (1));
        Put_Line ("Configuration loaded from " & Argument (1) & ".");
    end if;

    Print_Config;

    Validate_Config;

    if Server_Config.Is_Valid then
        Put_Line ("Server configuration is valid.");
        New_Line;
    else
        Put_Line ("Server configuration is invalid.");
        Set_Exit_Status (Failure);
        return;
    end if;

    Validate_Mime_File;

    Add_Handlers;

    if Argument_Count <= 1 then
        Put_Line ("Use the following synopsis to emulate a request:");
        Put_Line ("    ./Agemini_Config_Test default|PATH URL "
            & "[CERT_ISSUER CERT_TOKEN]");
        return;
    end if;

    Print_Url;

    Request.Initialise (Agemini.URLs.Sanitisers.Sanitise (Argument (2)));
    if Argument_Count >= 3 then
        Request.Set_TLS_Certificate (Parameters_To_Certificate_Info);
    end if;
    Response := Handlers.Get_Response (Request);

    Put_Line ("Response:");
    Put_Line (To_String (Response));

    New_Line;
    Print_Handler_Responses;
end Agemini_Config_Test;
