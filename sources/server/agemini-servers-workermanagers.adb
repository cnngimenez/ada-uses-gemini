--  agemini-servers-workermanagers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Agemini.Servers.Socketmanagers;
use Agemini.Servers.Socketmanagers;
with Agemini.Servers.Freeworkers;
use Agemini.Servers.Freeworkers;

package body Agemini.Servers.Workermanagers is

    procedure Add (Manager : in out Worker_Manager;
                   Socket_Index : Natural)
    is
        Free_Index : Natural;
    begin
        Free_Index := Free_Workers.Find_Free_Worker;
        if Free_Index = 0 then
            return;
        end if;

        Manager.Worker_List (Free_Index).Start (Sockets_Range (Socket_Index));
        --  Free_Workers.Mark_Busy (Workers_Range (Free_Index));
    end Add;

    procedure Initialise (Manager : in out Worker_Manager;
                          Handlers : Handler_Manager)
    is
    begin
        for I in Manager.Worker_List'Range loop
            Manager.Worker_List (I).Initialise (Handlers, I);
        end loop;
        Free_Workers.Initialise;
        --  Manager.Sockets := Sockets'Access;
    end Initialise;
end Agemini.Servers.Workermanagers;
