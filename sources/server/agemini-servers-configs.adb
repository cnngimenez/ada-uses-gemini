--  agemini-servers-configs.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------
with Ada.Directories;
use Ada.Directories;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;
with Ada.Strings;
use Ada.Strings;
with Ada.Characters.Handling;
use Ada.Characters.Handling;
with Ada.Characters.Latin_1;
use Ada.Characters;

package body Agemini.Servers.Configs is

    function Get_Cert_File (Config : Config_Type) return Unbounded_String is
        (Config.Cert_File);

    function Get_Cert_File (Config : Config_Type) return String is
        (To_String (Config.Cert_File));

    function Get_CGI_Directory (Config : Config_Type) return Unbounded_String
        is (Config.CGI_Directory);

    function Get_CGI_Directory (Config : Config_Type) return String is
        (To_String (Config.CGI_Directory));

    function Get_CGI_Enabled (Config : Config_Type) return Boolean is
        (Config.CGI_Enabled);

    function Get_Hostname (Config : Config_Type) return Unbounded_String is
        (Config.Hostname);

    function Get_Hostname (Config : Config_Type) return String is
        (To_String (Config.Hostname));

    function Get_IP_Address (Config : Config_Type) return Unbounded_String is
        (Config.IP_Address);

    function Get_IP_Address (Config : Config_Type) return String is
        (To_String (Config.IP_Address));

    function Get_Key_File (Config : Config_Type) return Unbounded_String is
        (Config.Key_File);

    function Get_Key_File (Config : Config_Type) return String is
        (To_String (Config.Key_File));

    function Get_Listing_Enabled (Config : Config_Type) return Boolean is
        (Config.Listing_Enabled);

    function Get_Mime_File (Config :  Config_Type) return Unbounded_String is
        (Config.Mime_File);

    function Get_Mime_File (Config :  Config_Type) return String is
        (To_String (Config.Mime_File));

    function Get_Port (Config : Config_Type) return Port_Type is
        (Config.Port);

    function Get_Root_Path (Config : Config_Type) return Unbounded_String is
        (Config.Root_Path);

    function Get_Root_Path (Config : Config_Type) return String is
        (To_String (Config.Root_Path));

    function Get_Secure_Path (Config : Config_Type) return Unbounded_String is
        (Config.Secure_Path);

    function Get_Secure_Path (Config : Config_Type) return String is
        (To_String (Config.Secure_Path));

    function Get_Sock_Addr_Type (Config : Config_Type) return Sock_Addr_Type is
        Address : Sock_Addr_Type;
    begin
        Address.Addr := Inet_Addr (To_String (Config.IP_Address));
        Address.Port := Config.Port;
        return Address;
    end Get_Sock_Addr_Type;

    function Get_TLS_Config (Config : Config_Type)
             return TLS.Configure.Config
    is
        Conf : TLS.Configure.Config;
    begin
        Conf.Verify_Client_Cert := TLS.Configure.Optional;
        Conf.Verify_Certs := False;
        Conf.Verify_Expirys := False;

        Conf.Cert_File := Config.Cert_File;
        Conf.Key_File := Config.Key_File;
        return Conf;
    end Get_TLS_Config;

    function Get_Valid_Tokens (Config : Config_Type) return Token_Vector_Type
        is (Config.Valid_Tokens);

    --  function Get_Valid_Tokens (Config : Config_Type) return String is
    --      (To_String (Config.Valid_Tokens));

    function Image (Config : Config_Type) return String is
        use Token_Vector_Package;
        procedure Add_Token (Token_Cursor : Cursor);

        Token_Image : Unbounded_String;

        procedure Add_Token (Token_Cursor : Cursor) is
        begin
            Append (Token_Image, "Valid Token: """
                & To_String (Element (Token_Cursor))
                & """" & Latin_1.LF);
        end Add_Token;
    begin
        Config.Valid_Tokens.Iterate (Add_Token'Access);

        return
        "IP Address : """ & To_String (Config.IP_Address) & """" & Latin_1.LF &
        "Port : " & Config.Port'Image & Latin_1.LF &
        "Root path : """ & To_String (Config.Root_Path) & """" & Latin_1.LF &
        "Hostname : """ & To_String (Config.Hostname) & """" & Latin_1.LF &

        "Cert file : """ & To_String (Config.Cert_File) & """" & Latin_1.LF &
        "Key File : """ & To_String (Config.Key_File) & """" & Latin_1.LF &

        "MIME File : """ & To_String (Config.Mime_File) & """" & Latin_1.LF &

        --  Secure directory handler config:
        "Secure Path : """ & To_String (Config.Secure_Path) & """"
            & Latin_1.LF & To_String (Token_Image) &
        --  "Valid Token : """ & To_String (Config.Valid_Tokens) & """"
        --      & Latin_1.LF &

        --  Directory handler config:
        "Listing Enabled : " & Config.Listing_Enabled'Image & Latin_1.LF &

        "CGI Enabled : " & Config.CGI_Enabled'Image & Latin_1.LF &
        "CGI Directory : """ & To_String (Config.CGI_Directory) & """"
            & Latin_1.LF;
    end Image;

    procedure Initialise_Default (Config : in out Config_Type) is
    begin
        Config.IP_Address := To_Unbounded_String ("127.0.0.1");
        Config.Port := 1965;
        Config.Hostname := To_Unbounded_String ("localhost");

        Config.Root_Path := To_Unbounded_String (
            Current_Directory & "/run/site");
        Config.Cert_File := To_Unbounded_String (
            Compose (Current_Directory & "/run/certs/", "cert", "pem"));
        Config.Key_File := To_Unbounded_String (
            Compose (Current_Directory & "/run/certs/", "key", "pem"));
        Config.Mime_File := To_Unbounded_String (
            Compose (Current_Directory & "/run/", "mime", "types"));

        --  Secure directory handler config:
        Config.Secure_Path := To_Unbounded_String ("/Secure");
        --  Config.Valid_Tokens := To_Unbounded_String ("");

        --  Directory handler config:
        Config.Listing_Enabled := False;

        Config.CGI_Directory := To_Unbounded_String ("cgi");
        Config.CGI_Enabled := False;
    end Initialise_Default;

    function Is_Cert_File_Valid (Config : Config_Type) return Boolean is
        Filepath : constant String := To_String (Config.Cert_File);
    begin
        return Exists (Filepath) and then Kind (Filepath) = Ordinary_File;
    end Is_Cert_File_Valid;

    function Is_Key_File_Valid (Config : Config_Type) return Boolean is
        Filepath : constant String := To_String (Config.Key_File);
    begin
        return Exists (Filepath) and then Kind (Filepath) = Ordinary_File;
    end Is_Key_File_Valid;

    function Is_Mime_File_Valid (Config : Config_Type) return Boolean is
        Filepath : constant String := To_String (Config.Mime_File);
    begin
        return Exists (Filepath) and then Kind (Filepath) = Ordinary_File;
    end Is_Mime_File_Valid;

    function Is_Root_Path_Valid (Config : Config_Type) return Boolean is
        Filepath : constant String := To_String (Config.Root_Path);
    begin
        return Exists (Filepath) and then Kind (Filepath) = Directory;
    end Is_Root_Path_Valid;

    function Is_Token_Valid (Config : Config_Type; Token : String)
        return Boolean
        is (Config.Valid_Tokens.Contains (To_Unbounded_String (Token)));

    function Is_Valid (Config : Config_Type) return Boolean is
    begin
        return Config.Is_Key_File_Valid
            and then Config.Is_Cert_File_Valid
            and then Config.Is_Mime_File_Valid
            and then Config.Is_Root_Path_Valid;
    end Is_Valid;

    procedure Load_File (Config : in out Config_Type; Path : String) is
        File : File_Type;

        procedure Assign_Value (Name : String; Value : String);
        function Is_Comment (Line : String) return Boolean;
        procedure Process_Line (Line : String);
        function Remove_Quotes (Value : String) return String;

        procedure Assign_Value (Name : String; Value : String) is
            Name_Downcase : constant String := To_Lower (Name);
            Value_Sanitised : constant String := Remove_Quotes (Value);
        begin
            if Name_Downcase = "ip address" then
                Config.IP_Address := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "port" then
                Config.Port := Port_Type'Value (Value_Sanitised);
            elsif Name_Downcase = "root path" then
                Config.Root_Path := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "hostname" then
                Config.Hostname := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "cert file" then
                Config.Cert_File := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "key file" then
                Config.Key_File := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "mime file" then
                Config.Mime_File := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "secure path" then
                Config.Secure_Path := To_Unbounded_String (Value_Sanitised);
            elsif Name_Downcase = "valid token" then
                Config.Valid_Tokens.Append
                    (To_Unbounded_String (To_Lower (Value_Sanitised)));
            elsif Name_Downcase = "listing enabled" then
                Config.Listing_Enabled := To_Lower (Value_Sanitised) = "true";
            elsif Name_Downcase = "cgi enabled" then
                Config.CGI_Enabled := To_Lower (Value_Sanitised) = "true";
            elsif Name_Downcase = "cgi directory" then
                Config.CGI_Directory := To_Unbounded_String (Value_Sanitised);
            end if;
        end Assign_Value;

        function Is_Comment (Line : String) return Boolean is
            I : Natural := 1;
        begin
            while I <= Line'Length and then Line (I) = ' ' loop
                I := I + 1;
            end loop;

            return I <= Line'Length and then Line (I) = '#';
        end Is_Comment;

        procedure Process_Line (Line : String) is
            Sep : Natural;
        begin
            if Is_Comment (Line) then
                return;
            end if;

            Sep := Index (Line, ":", 1);
            if Sep > 0 then
                Assign_Value (Trim (Line (Line'First .. Sep - 1),
                                    Ada.Strings.Both),
                              Trim (Line (Sep + 1 .. Line'Last),
                                    Ada.Strings.Both));
            end if;
        end Process_Line;

        function Remove_Quotes (Value : String) return String is
        begin
            if Value (Value'First) = '"' and then Value (Value'Last) = '"'
            then
                return Value (Value'First + 1 .. Value'Last - 1);
            else
                return Value;
            end if;
        end Remove_Quotes;
    begin
        Config.Initialise_Default;

        Open (File, In_File, Path);
        while not End_Of_File (File) loop
            Process_Line (Get_Line (File));
        end loop;
        Close (File);
    end Load_File;

end Agemini.Servers.Configs;
