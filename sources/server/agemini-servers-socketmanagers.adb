--  agemini-servers-socketmanagers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Exceptions;
use Ada.Exceptions;
with System.Address_Image;

package body Agemini.Servers.Socketmanagers is

    protected body Socket_Manager is

        entry Accept_Connection (Server_TLS : in out Server_Context;
                                 Server_Socket : Socket_Type;
                                 Address : in out Sock_Addr_Type;
                                 Index : out Natural)
            when Has_Free_Sockets
        is
            I : Natural;
        begin
            I := Find_Free_Socket_Index;
            --  if I = 0 then
            --      --  raise exception?
            --      Index := 0;
            --      return;
            --  end if;

            Put_Line ("SM: Waiting for connection");
            Put_Line ("SM: Available new free socket:" & I'Image);

            Accept_Socket (Server_Socket,
                           Connection_Socket_List (Sockets_Range (I)),
                           Address);
            TLS.Contexts.Server.Accept_TLS
                (Server_TLS,
                 Connection_Socket_List (Sockets_Range (I)),
                 Connection_TLS_List (Sockets_Range (I)));
            Free_Socket (Sockets_Range (I)) := False;
            Index := I;

        exception
            when Error : TLS.Connect_Error =>
                Put_Line ("SM: Connection error: "
                    & Exception_Message (Error));
                Put_Line ("SM: Client may close the connection abruptly.");
                Put_Line ("SM: Closing socket " & I'Image
                    & " and trying to continue serving.");
                Flush;

                Connection_TLS_List (Sockets_Range (I)).Close;
                Close_Socket (Connection_Socket_List (Sockets_Range (I)));
                Free_Socket (Sockets_Range (I)) := True;
                Index := 0;
            when Error : others =>
                Put_Line ("SM: Error: " & Exception_Message (Error));
                Put_Line ("SM: Closing socket " & I'Image
                    & " and trying to continue serving.");
                Flush;

                Connection_TLS_List (Sockets_Range (I)).Close;
                Close_Socket (Connection_Socket_List (Sockets_Range (I)));
                Free_Socket (Sockets_Range (I)) := True;
                Index := 0;
        end Accept_Connection;

        procedure Close (Index : Sockets_Range)
        is
        begin
            if Free_Socket (Index) then
                Put_Line ("SM: Won't close socket " & Index'Image
                    & " because it is free!");
                return;
            end if;

            Connection_TLS_List (Index).Close;
            Close_Socket (Connection_Socket_List (Index));
            Free_Socket (Index) := True;
            Put_Line ("SM: Socket " & Index'Image & " Closed.");

        exception
            when Error : others =>
                Put_Line ("SM: Error: " & Exception_Message (Error));
                Put_Line ("SM: Tried to close the socket " & Index'Image & "! "
                & "I Don't know what to do!,  I'll just pretend this Socket "
                & "is free and see whats happens...");

                Free_Socket (Index) := True;
        end Close;

        function Find_Free_Socket_Index return Natural is
            Found : Boolean := False;
            I : Natural := 1;
        begin
            while I <= Max_Sockets and then not Found loop
                Found := Free_Socket (Sockets_Range (I));
                I := I + 1;
            end loop;

            if Found then
                return I - 1;
            else
                return 0;
            end if;
        end Find_Free_Socket_Index;

        procedure Get_Sockets (Index : Sockets_Range;
                               Socket : out Socket_Access;
                               TLS_Socket : out Server_Context_Access;
                               Valid : out Boolean) is
        begin
            Socket := Connection_Socket_List (Index)'Access;
            TLS_Socket := Connection_TLS_List (Index)'Access;
            Valid := not Free_Socket (Index);
        end Get_Sockets;

        function Has_Free_Sockets return Boolean
            is (Find_Free_Socket_Index > 0);

        function Image return String is
            S : String (1 .. Max_Sockets);
        begin
            for I in 1 .. Max_Sockets loop
                S (I) :=
                    (if Free_Socket (Sockets_Range (I)) then 'F' else 'B');
            end loop;
            return S & " | Max: " & Max_Sockets'Image;
        end Image;

        procedure Initialise is
        begin
            Put_Line ("SM: Socket manager address: "
                & System.Address_Image (Socket_Manager'Address));
            for I of Free_Socket loop
                I := True;
            end loop;
        end Initialise;

        function Is_Configured (Index : Sockets_Range) return Boolean
            is (TLS.Contexts.Is_Configued
                (Context (Connection_TLS_List (Index))));

        function Is_Connected (Index : Sockets_Range) return Boolean
            is (TLS.Contexts.Is_Connected
                (Context (Connection_TLS_List (Index))));

        function Is_Initialised (Index : Sockets_Range) return Boolean
            is (TLS.Contexts.Is_Initialized
                (Context (Connection_TLS_List (Index))));

        procedure Receive_Line (Index : Sockets_Range;
                                Line : out Unbounded_String)
        is
            C1, C2 : Character := ' ';
            I : Natural := 0;
        begin
            Put_Line ("SM: Receiving line from socket " & Index'Image);
            Line := To_Unbounded_String ("");
            while I <= 1024 and then C1 /= ASCII.CR and then C2 /= ASCII.LF
            loop
                C1 := C2;

                Character'Read (Connection_TLS_List (Index)'Access, C2);
                Append (Line, C2);

                I := I + 1;
            end loop;

            if I > 1024 then
                Line := Null_Unbounded_String;
                return;
            end if;

            Delete (Line, Length (Line) - 1, Length (Line));

        end Receive_Line;

        procedure Receive_Request (Index : Sockets_Range;
                                   Request : out Agemini.Requests.Request_Type)
        is
            use type GNAT.Sockets.Family_Type;
            use type GNAT.Sockets.Family_Inet_4_6;

            Request_String : Unbounded_String;
            Peer_Addr : GNAT.Sockets.Sock_Addr_Type;
        begin
            if Free_Socket (Index) then
                return;
            end if;
            Put_Line ("SM: Socket " & Index'Image & " is receiving request");

            --  Gemini specification only states that there should be a
            --  request line.
            Receive_Line (Index, Request_String);
            Put_Line ("SM: Received from socket " & Index'Image & ":"
                & To_String (Request_String));
            Request.Initialise (Request_String);

            --  We add more information to the request type for the Handlers.
            --  The handlers may want to know about the TLS certificate and The
            --  client Host.

            --  Request.Set_TLS_Connection (Get_Connection_Info
            --      (Connection_TLS_List (Index)));
            --  ^^ This one gives failed precondition error tls-context.ads:220
            --  Uncomment this when tlsada has been Fixed.

            if Peer_Certificate_Provided (Connection_TLS_List (Index)) then
                Put_Line ("SM: Certificate provided!");
                Request.Set_TLS_Certificate (Get_Certificate_Info
                    (Connection_TLS_List (Index)));
            end if;

            Peer_Addr := GNAT.Sockets.Get_Peer_Name
                (Connection_Socket_List (Index));
            if Peer_Addr.Family in Family_Inet_4_6 then
                --  I don't think it would be any other Family,
                --  but just in case...
                Request.Set_Host (GNAT.Sockets.Image (Peer_Addr.Addr));
                Request.Set_Port (TLS.Contexts.Port_Number (Peer_Addr.Port));
            end if;

        end Receive_Request;

        procedure Register_Busy (Index : Sockets_Range) is
        begin
            Free_Socket (Index) := False;
        end Register_Busy;

        procedure Send (Index : Sockets_Range; Message : String)
        is
        begin
            if Free_Socket (Index) then
                return;
            end if;
            Put_Line ("SM: Socket " & Index'Image
                & " is sending: " & Message);

            String'Write (Connection_TLS_List (Index)'Access, Message);
        end Send;

        procedure Send (Index : Sockets_Range; Response : Response_Type)
        is
        begin
            if Free_Socket (Index) then
                return;
            end if;
            Put_Line ("SM: Socket " & Index'Image
                & " is sending: " & To_String (Response));

            String'Write (Connection_TLS_List (Index)'Access,
                          To_String (Response));
        end Send;

    end Socket_Manager;
end Agemini.Servers.Socketmanagers;
