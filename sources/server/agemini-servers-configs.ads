--  agemini-servers-configs.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;
with GNAT.Sockets;
use GNAT.Sockets;

with TLS.Configure;
use TLS.Configure;

package Agemini.Servers.Configs is

    package Token_Vector_Package is new Ada.Containers.Vectors
        (Index_Type => Positive,
         Element_Type => Unbounded_String);
    subtype Token_Vector_Type is Token_Vector_Package.Vector;

    type Config_Type is tagged private;

    procedure Initialise_Default (Config : in out Config_Type);
    procedure Load_File (Config : in out Config_Type; Path : String);

    function Get_IP_Address (Config : Config_Type) return Unbounded_String;
    function Get_IP_Address (Config : Config_Type) return String;
    function Get_Port (Config : Config_Type) return Port_Type;
    function Get_Root_Path (Config : Config_Type) return Unbounded_String;
    function Get_Root_Path (Config : Config_Type) return String;
    function Get_Hostname (Config : Config_Type) return Unbounded_String;
    function Get_Hostname (Config : Config_Type) return String;
    function Get_Cert_File (Config : Config_Type) return Unbounded_String;
    function Get_Cert_File (Config : Config_Type) return String;
    function Get_Key_File (Config : Config_Type) return Unbounded_String;
    function Get_Key_File (Config : Config_Type) return String;
    function Get_Mime_File (Config :  Config_Type) return Unbounded_String;
    function Get_Mime_File (Config :  Config_Type) return String;

    function Get_Secure_Path (Config : Config_Type) return Unbounded_String;
    function Get_Secure_Path (Config : Config_Type) return String;
    function Get_Valid_Tokens (Config : Config_Type) return Token_Vector_Type;

    function Get_CGI_Directory (Config : Config_Type) return Unbounded_String;
    function Get_CGI_Directory (Config : Config_Type) return String;
    function Get_CGI_Enabled (Config : Config_Type) return Boolean;

    function Get_Listing_Enabled (Config : Config_Type) return Boolean;

    function Get_Sock_Addr_Type (Config : Config_Type) return Sock_Addr_Type;
    function Get_TLS_Config (Config : Config_Type) return TLS.Configure.Config;

    function Is_Valid (Config : Config_Type) return Boolean;
    function Is_Key_File_Valid (Config : Config_Type) return Boolean;
    function Is_Cert_File_Valid (Config : Config_Type) return Boolean;
    function Is_Mime_File_Valid (Config : Config_Type) return Boolean;
    function Is_Root_Path_Valid (Config : Config_Type) return Boolean;

    function Is_Token_Valid (Config : Config_Type; Token : String)
        return Boolean;

    function Image (Config : Config_Type) return String;
    --  Return a string representation that can be loaded with Load_File.
private

    type Config_Type is tagged record
        IP_Address : Unbounded_String;
        Port : Port_Type;

        Root_Path : Unbounded_String;
        Hostname : Unbounded_String;

        Cert_File : Unbounded_String;
        Key_File : Unbounded_String;

        Mime_File : Unbounded_String;

        CGI_Directory : Unbounded_String;
        CGI_Enabled : Boolean;

        --  Directory_Handler config:
        Listing_Enabled : Boolean;

        --  Secure_Directory_Handler config:
        Secure_Path : Unbounded_String;
        Valid_Tokens : Token_Vector_Type;
    end record;

end Agemini.Servers.Configs;
