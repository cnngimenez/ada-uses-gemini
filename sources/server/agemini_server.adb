--  agemini_server.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

--
--  This program is largely based on the server_example.adb from
--  https://git.sr.ht/~nytpu/Tlsada
--
with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Exceptions;
use Ada.Exceptions;
with GNAT.Sockets;
use GNAT.Sockets;
with System.Address_Image;

with TLS.Contexts;
with TLS.Contexts.Server;

--  Handlers
with Agemini.Servers.SecurityHandlers;
use Agemini.Servers.SecurityHandlers;
with Agemini.Servers.Filehandlers;
use Agemini.Servers.Filehandlers;
with Agemini.Servers.Directoryhandlers;
use Agemini.Servers.Directoryhandlers;
with Agemini.Servers.Secure_DirectoryHandlers;
use Agemini.Servers.Secure_DirectoryHandlers;
with Agemini.Servers.CGI_Handlers;
use Agemini.Servers.CGI_Handlers;
with Agemini.Servers.Handlermanagers;
use Agemini.Servers.Handlermanagers;

with Agemini.Servers.Configs;
use Agemini.Servers.Configs;
with Agemini.Servers.Workermanagers;
use Agemini.Servers.Workermanagers;
with Agemini.Servers.Main_Managers;
use Agemini.Servers.Main_Managers;
with Agemini.Servers.Socketmanagers;
use Agemini.Servers.Socketmanagers;

procedure Agemini_Server is
    procedure Accept_Connection;
    procedure Add_Handlers;
    procedure Validate_Config;

    Server_Config : Config_Type;
    Handlers : Handler_Manager;
    Fhandler : aliased File_Handler;
    Chandler : aliased CGI_Handler;
    Dhandler : aliased Directory_Handler;
    Sdhandler : aliased Secure_Directory_Handler;
    SecHandler : aliased Security_Handler;

    Address : Sock_Addr_Type;
    Server_Socket : Socket_Type;
    Server_TLS : aliased TLS.Contexts.Server.Server_Context;

    Client_Socket : Socket_Access;
    Client_TLS_Socket : Server_Context_Access;

    --  Sockets : aliased Socket_Manager;
    Socket_Index : Natural := 0;

    procedure Accept_Connection is
        Valid : Boolean;
    begin
        Socket_Index := Socket_Manager.Find_Free_Socket_Index;
        Socket_Manager.Get_Sockets (Sockets_Range (Socket_Index),
                                    Client_Socket, Client_TLS_Socket,
                                    Valid);
        if Valid then
            --  This socket are being used!
            return;
        end if;

        Accept_Socket (Server_Socket, Client_Socket.all, Address);
        TLS.Contexts.Server.Accept_TLS (Server_TLS,
                                        Client_Socket.all,
                                        Client_TLS_Socket.all);
    end Accept_Connection;

    procedure Add_Handlers is
    begin
        SecHandler.Initialise (Server_Config);
        Sdhandler.Initialise (Server_Config);
        Fhandler.Initialise (Server_Config);
        Chandler.Initialise (Server_Config);
        Dhandler.Initialise (Server_Config);

        Handlers.Add_Handler (SecHandler'Unchecked_Access);
        Handlers.Add_Handler (Sdhandler'Unchecked_Access);
        Handlers.Add_Handler (Fhandler'Unchecked_Access);
        Handlers.Add_Handler (Chandler'Unchecked_Access);
        Handlers.Add_Handler (Dhandler'Unchecked_Access);
    end Add_Handlers;

    procedure Validate_Config is
    begin
        if not Server_Config.Is_Key_File_Valid then
            Put_Line ("- Key file not found or invalid");
            Put_Line ("  " & Server_Config.Get_Key_File);
        end if;

        if not Server_Config.Is_Cert_File_Valid then
            Put_Line ("- Cert file not found or is invalid:");
            Put_Line ("  " & Server_Config.Get_Cert_File);
        end if;

        if not Server_Config.Is_Mime_File_Valid then
            Put_Line ("- MIME file is not found or is Invalid");
            Put_Line ("  " & Server_Config.Get_Mime_File);
        end if;

        if not Server_Config.Is_Root_Path_Valid then
            Put_Line ("- Root path is not Valid.");
            Put_Line ("  " & Server_Config.Get_Root_Path);
        end if;
    end Validate_Config;

begin
    if Argument_Count = 0 then
        Server_Config.Initialise_Default;
        Put_Line ("Using default configuration.");
        --  Put_Line ("See ./agemini_server -h for more information.");
    else
        Server_Config.Load_File (Argument (1));
        Put_Line ("Configuration loaded from " & Argument (1) & ".");
    end if;

    Validate_Config;

    if Server_Config.Is_Valid then
        Put_Line ("Server configuration is valid.");
    else
        Put_Line ("Server configuration is invalid.");
        Set_Exit_Status (Failure);
        return;
    end if;

    Add_Handlers;

    Put_Line ("M: Binding Socket to '" & Server_Config.Get_IP_Address & "'");

    Create_Socket (Server_Socket);
    Set_Socket_Option (Server_Socket, Socket_Level, (Reuse_Address, True));
    Address := Server_Config.Get_Sock_Addr_Type;
    Bind_Socket (Server_Socket, Address);
    Listen_Socket (Server_Socket);

    Put_Line ("M: Configure TLS Socket");
    Server_TLS.Configure (Server_Config.Get_TLS_Config);

    Put_Line ("M: Initialising Sockets");
    Socket_Manager.Initialise;
    Put_Line ("M: Initialising Worker_Manager");
    Main_Worker_Manager.Initialise (Handlers);

    Put_Line ("M: Socket manager address: "
        & System.Address_Image (Socket_Manager'Address));
    loop
        Put_Line ("M: Listening for new connections...");
        Put_Line ("M: Sockets: " & Socket_Manager.Image);
        Put_Line ("M: Socket manager address: "
            & System.Address_Image (Socket_Manager'Address));
        --  while not Socket_Manager.Has_Free_Sockets loop
        --    Put_Line ("M: Sockets: " & Socket_Manager.Image);
        --    Put_Line ("M: No free sockets... waiting 1 second for them...");
        --    delay 1.0;
        --  end loop;

        begin
            while not Socket_Manager.Has_Free_Sockets loop
                delay 5.0;
            end loop;

            Accept_Connection;

            Put_Line ("M: Got connection from socket "
                & Socket_Index'Image
                & " '" & Image (Address.Addr) & "'!");
            Flush;

            Socket_Manager.Register_Busy (Sockets_Range (Socket_Index));
            Main_Worker_Manager.Add (Socket_Index);
        exception
            when Error : TLS.Connect_Error =>
                Put_Line ("M: Connection error: " & Exception_Message (Error));
                Put_Line ("M: Client may close the connection abruptly.");
                Put_Line ("M: Closing socket " & Socket_Index'Image
                    & " and trying to continue serving.");
                Flush;
                if Socket_Index /= 0 then
                    Socket_Manager.Close (Sockets_Range (Socket_Index));
                end if;
            when Error : others =>
                Put_Line ("M: Error: " & Exception_Message (Error));
                Put_Line ("M: Closing socket " & Socket_Index'Image
                    & " and trying to continue serving.");
                Flush;
                if Socket_Index /= 0 then
                    Socket_Manager.Close (Sockets_Range (Socket_Index));
                end if;
        end;
    end loop;

    --  --  Unreachable code:
    --  Server_TLS.Close;
    --  Close_Socket (Server_Socket);
end Agemini_Server;
