--  agemini-servers-freeworkers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Agemini.Servers.Constants;
use Agemini.Servers.Constants;

package Agemini.Servers.Freeworkers is

    type Status_Type is (Free, Busy);
    type Workers_Range is range 1 .. Max_Workers;
    type Status_List_Type is array (Workers_Range) of Status_Type;

    protected Free_Workers is
        procedure Initialise;

        procedure Mark_Free (Index : Workers_Range);
        procedure Mark_Busy (Index : Workers_Range);

        function Find_Free_Worker return Natural;
        --  Return the index of a free Worker.
        --
        --  Returns 0 if no free worker is found.
    private
        Status_List : Status_List_Type;
    end Free_Workers;

end Agemini.Servers.Freeworkers;
