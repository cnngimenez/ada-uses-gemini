--  agemini-urls-sanitisers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package Agemini.URLs.Sanitisers is

    function To_Local_Path (Url, Root_Path : String;
                            Index_Default : String := "")
        return String;
    --  Return the local path that points the provided URL.
    --
    --  No checks are performed, just the Path match.

    function Sanitise (Url : String) return String;
    --  Apply all filter implemented here.

    function Sanitise_Double_Dash (Url : String) return String;
    --  Remove double dash ("//") in the URL path.

    function Sanitise_Dots (Url : String) return String;
    --  Remove all "/./" Subpaths.

    function Sanitise_Double_Dots (Url : String) return String;
    --  Remove all "/../" subpaths that are not Valid.
    --
    --  When there is a subdirectory, like "/b/../", both of them should Be
    --  deleted.
    --  This kind of subpaths can point outside the main directory.  These
    --  should be removed to protect the system Privacy.

    --  --------------------------------------------------
    --  Syntax Validation
    --  --------------------------------------------------

    function Is_Syntax_Valid (Url : String) return Boolean;
    --  Check if this is a valid formatted URL.

    function Is_Scheme_Valid (Str : String) return Boolean;

    function Is_Authority_Valid (Str : String) return Boolean;
    function Is_Port_Valid (Str : String) return Boolean;
    function Is_Regname_Valid (Str : String) return Boolean;
    function Is_Ipv4_Valid (Str : String) return Boolean;
    function Is_Ipv6_Valid (Str : String) return Boolean;
    function Is_Host_Valid (Str : String) return Boolean;
    function Is_Userinfo_Valid (Str : String) return Boolean;

    --  function Is_Path_Valid (Str : String) return Boolean;
    function Is_Query_Valid (Str : String) return Boolean;
    function Is_Fragment_Valid (Str : String) return Boolean;

private

    function Replace_All (Base : String; Find : String; Replacement : String)
        return String;
    --  Look for Find subpath in Base and replace it with Replacement.
    --
    --  Find should be a path, that is it should be found in Base as Find & "/"
    --  or Find should be the suffix of Base.
    --
    --  For example: if Find is "/..", then Replace_All will look for all
    --  substrings "/../", or for "/.." only at the end of Base.

end Agemini.URLs.Sanitisers;
