--  agemini-dezhemini_infos.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Environment_Variables;
with Ada.Characters.Latin_1;

package body Agemini.Dezhemini_Infos is
    use Ada.Environment_Variables;

    function Get_Parameter (Self : Cgi_Dezhemini_Info;
                            Name : Parameter_Name) return Unbounded_String is
    begin
        case Name is
        when Document_Root =>
            return Self.Document_Root;
        when Gateway_Interface =>
            return Self.Gateway_Interface;
        when Gemini_Url =>
            return Self.Gemini_Url;
        when Path_Info =>
            return Self.Path_Info;
        when Path_Translated =>
            return Self.Path_Translated;
        when Query_String =>
            return Self.Query_String;
        when Script_Name =>
            return Self.Script_Name;
        when Server_Name =>
            return Self.Server_Name;
        when Server_Protocol =>
            return Self.Server_Protocol;
        when Auth_Type =>
            return To_Unbounded_String (Self.Auth_Type'Image);
        when Remote_User =>
            if Self.Auth_Type = Certificate then
                return Self.Remote_User;
            else
                return Null_Unbounded_String;
            end if;
        when Tls_Client_Hash  =>
            if Self.Auth_Type = Certificate then
                return Self.Tls_Client_Hash;
            else
                return Null_Unbounded_String;
            end if;
        end case;
    end Get_Parameter;

    function Get_Parameter (Self : Cgi_Dezhemini_Info;
                            Name : Parameter_Name) return String is
        (To_String (Get_Parameter (Self, Name)));

    procedure Initialise_Common_Info (Self : in out Cgi_Dezhemini_Info) is
    begin
        Self.Document_Root :=
          To_Unbounded_String (Value ("DOCUMENT_ROOT", ""));
        Self.Gateway_Interface :=
          To_Unbounded_String (Value ("GATEWAY_INTERFACE", ""));
        Self.Gemini_Url :=
          To_Unbounded_String (Value ("GEMINI_URL", ""));
        Self.Path_Info :=
          To_Unbounded_String (Value ("PATH_INFO", ""));
        Self.Path_Translated :=
          To_Unbounded_String (Value ("PATH_TRANSLATED", ""));
        Self.Query_String :=
          To_Unbounded_String (Value ("QUERY_STRING", ""));
        Self.Script_Name :=
          To_Unbounded_String (Value ("SCRIPT_NAME", ""));
        Self.Server_Name :=
          To_Unbounded_String (Value ("SERVER_NAME", ""));
        Self.Server_Protocol :=
          To_Unbounded_String (Value ("SERVER_PROTOCOL", ""));
        Self.Server_Software :=
          To_Unbounded_String (Value ("SERVER_SOFTWARE", ""));
    end Initialise_Common_Info;

    function Initialise_From_System return Cgi_Dezhemini_Info is
    begin
        if Value ("AUTH_TYPE", "") = "CERTIFICATE" then
            declare
                Self : Cgi_Dezhemini_Info (Auth_Type => Certificate);
            begin
                Self.Initialise_Common_Info;
                Self.Remote_User :=
                  To_Unbounded_String (Value ("REMOTE_USER", ""));
                Self.Tls_Client_Hash :=
                  To_Unbounded_String (Value ("TLS_CLIENT_HASH", ""));
                return Self;
            end;
        else
            declare
                Self : Cgi_Dezhemini_Info (Auth_Type => None);
            begin
                Self.Initialise_Common_Info;
                return Self;
            end;
        end if;
    end Initialise_From_System;

    function Is_Authenticated (Self : Cgi_Dezhemini_Info) return Boolean is
    begin
        return Self.Auth_Type = Certificate;
    end Is_Authenticated;

    function Is_Authenticated (Self : Cgi_Dezhemini_Info;
                               Certificate_Hash : String) return Boolean is
    begin
        return Self.Auth_Type = Certificate and then
          To_String (Self.Tls_Client_Hash) = Certificate_Hash;
    end Is_Authenticated;

    function Is_Authenticated (Self : Cgi_Dezhemini_Info;
                               Accepted_Auths : Authentication_Vector)
                              return Boolean is
    begin
        return Self.Auth_Type = Certificate and then
          Member (Accepted_Auths, To_String (Self.Tls_Client_Hash));
    end Is_Authenticated;

    function To_String (Self : Cgi_Dezhemini_Info) return String is
        use Ada.Characters;

        function Certificate_To_String return String;

        Eol : constant Character := Latin_1.LF;

        function Certificate_To_String return String is
        begin
            if Self.Auth_Type = Certificate then
                return "Auth_Type: CERTIFICATE" & Eol &
                  "Remote_User: " & To_String (Self.Remote_User) & Eol &
                  "Tls_Client_Hash: " & To_String (Self.Tls_Client_Hash) & Eol;
            else
                return "Client certificate was not supplied.";
            end if;
        end Certificate_To_String;

    begin
        return
          "Document_Root: " & To_String (Self.Document_Root) & Eol &
          "Gateway_Interface: " & To_String (Self.Gateway_Interface) & Eol &
          "Gemini_Url: " & To_String (Self.Gemini_Url) & Eol &
          "Path_Info: " & To_String (Self.Path_Info) & Eol &
          "Path_Translated: " & To_String (Self.Path_Translated) & Eol &
          "Query_String: " & To_String (Self.Query_String) & Eol &
          "Script_Name: " & To_String (Self.Script_Name) & Eol &
          "Server_Name: " & To_String (Self.Server_Name) & Eol &
          "Server_Protocol: " & To_String (Self.Server_Protocol) & Eol &
          "Server_Software: " & To_String (Self.Server_Software) & Eol &
          Certificate_To_String & Eol;
    end To_String;

end Agemini.Dezhemini_Infos;
