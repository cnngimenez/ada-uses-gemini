--  agemini-urls-sanitisers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Directories;
with Ada.Text_IO; use Ada.Text_IO;
with GNAT.Regpat;

package body Agemini.URLs.Sanitisers is

    Unreserved : constant String := "[[:alpha:][:digit:]-\._~]";
    --  unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
    Pct_Encoded : constant String := "%[[:xdigit:]][[:xdigit:]]";
    --  pct-encoded = "%" HEXDIG HEXDIG
    Sub_Delims : constant String := "[!$&'\(\)\*\+,;=]";
    --  sub-delims  = "!" / "$" / "&" / "'" / "(" / ")"
    --                / "*" / "+" / "," / ";" / "="
    Pchar : constant String := "(" & Unreserved
        & "|" & Pct_Encoded
        & "|" & Sub_Delims
        & "|[:@])";
    --  pchar         = unreserved / pct-encoded / sub-Delims / ":" / "@"

    function Is_Authority_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "[[:alpha:]][[:alnum:]+-.]*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  authority   = [ userinfo "@" ] host [ ":" port ]
        return Match (Matcher, Str);
    end Is_Authority_Valid;

    function Is_Fragment_Valid (Str : String) return Boolean
        renames Is_Query_Valid;
    --  fragment    = *( Pchar / "/" / "?" )

    function Is_Host_Valid (Str : String) return Boolean is
    begin
        --  host        = IP-literal / IPv4address / reg-Name
        --  IP-literal = "[" ( IPv6address / IPvFuture  ) "]"
        --  IPvFuture  = "v" 1*HEXDIG "." 1*( unreserved / sub-Delims / ":" )
        return True;
    end Is_Host_Valid;

    function Is_Ipv4_Valid (Str : String) return Boolean is
    begin
        --  IPv4address = dec-octet "." dec-octet "." dec-octet "." dec-octet
        --  dec-octet   = DIGIT                 ; 0-9
        --              / %x31-39 DIGIT         ; 10-99
        --              / "1" 2DIGIT            ; 100-199
        --              / "2" %x30-34 DIGIT     ; 200-249
        --              / "25" %x30-35          ; 250-255
        return True;
    end Is_Ipv4_Valid;

    function Is_Ipv6_Valid (Str : String) return Boolean is
    begin
        --  IPv6address =                            6( h16 ":" ) ls32
        --              /                       "::" 5( h16 ":" ) ls32
        --              / [               h16 ] "::" 4( h16 ":" ) ls32
        --              / [ *1( h16 ":" ) h16 ] "::" 3( h16 ":" ) ls32
        --              / [ *2( h16 ":" ) h16 ] "::" 2( h16 ":" ) ls32
        --              / [ *3( h16 ":" ) h16 ] "::"    h16 ":"   ls32
        --              / [ *4( h16 ":" ) h16 ] "::"              ls32
        --              / [ *5( h16 ":" ) h16 ] "::"              h16
        --              / [ *6( h16 ":" ) h16 ] "::"

        --  ls32        = ( h16 ":" h16 ) / IPv4address
        --              ; least-significant 32 bits of address

        --  h16         = 1*4HEXDIG
        --              ; 16 bits of address represented in hexadecimal
        return True;
    end Is_Ipv6_Valid;

    function Is_Port_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "[[:digit:]]*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  port        = *DIGIT
        return Match (Matcher, Str);
    end Is_Port_Valid;

    function Is_Query_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "(" & Pchar & "|/|\?)*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  query       = *( pchar / "/" / "?" )
        return Match (Matcher, Str);
    end Is_Query_Valid;

    function Is_Regname_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "(" & Unreserved
            & "|" & Pct_Encoded
            & "|" & Sub_Delims & ")*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  reg-name    = *( unreserved / pct-encoded / sub-delims )
        return Match (Matcher, Str);
    end Is_Regname_Valid;

    function Is_Scheme_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "[[:alpha:]][[:alnum:]+-.]*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  scheme      = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." )
        return Match (Matcher, Str);
    end Is_Scheme_Valid;

    function Is_Syntax_Valid (Url : String) return Boolean is
    begin
      --  URI         = scheme ":" hier-part [ "?" query ] [ "#" fragment ]

      --  hier-part   = "//" authority path-abempty
      --              / path-absolute
      --              / path-rootless
      --              / path-empty
        --  TBD
        return True;
    end Is_Syntax_Valid;

    function Is_Userinfo_Valid (Str : String) return Boolean is
        use GNAT.Regpat;
        Regexp : constant String := "(" & Unreserved
            & "|" & Pct_Encoded
            & "|" & Sub_Delims
            & "|:)*";
        Matcher : constant Pattern_Matcher := Compile (Regexp);
    begin
        --  userinfo    = *( unreserved / pct-encoded / sub-Delims / ":" )
        return Match (Matcher, Str);
    end Is_Userinfo_Valid;

    function Replace_All (Base : String; Find : String; Replacement : String)
        return String
    is
        Ubase : Unbounded_String := To_Unbounded_String (Base);
        I : Natural;

        procedure Next_Find;

        procedure Next_Find is
        begin
            I := Index (Ubase, Find, 1);
            while I > 0 loop
                exit when I + Find'Length >= Length (Ubase)
                    or else Element (Ubase, I + Find'Length - 1) = '/'
                    or else Element (Ubase, I + Find'Length) = '/';
                I := Index (Ubase, Find, I + 1);
            end loop;
        end Next_Find;
    begin
        Next_Find;

        while I > 0 loop
            Replace_Slice (Ubase, I, I + Find'Length - 1, Replacement);
            --  Put_Line (To_String (Ubase)); Flush;
            Next_Find;
        end loop;

        return To_String (Ubase);
    end Replace_All;

    function Sanitise (Url : String) return String is
    begin
        return Sanitise_Double_Dash
            (Sanitise_Dots
                (Sanitise_Double_Dots
                    (Sanitise_Double_Dash (Url))));
    end Sanitise;

    function Sanitise_Dots (Url : String) return String is
        Path : constant String := Get_Path (Url);
        Prefix : constant String :=
            Get_Scheme (Url) & "://" & Get_Authority (Url);
        Suffix : constant String :=
            Url (Url'First + Prefix'Length + Path'Length .. Url'Last);
    begin
        return Prefix & Replace_All (Path, "/.", "/") & Suffix;
    end Sanitise_Dots;

    function Sanitise_Double_Dash (Url : String) return String is
        Path : constant String := Get_Path (Url);
        Prefix : constant String :=
            Get_Scheme (Url) & "://" & Get_Authority (Url);
        Suffix : constant String :=
            Url (Url'First + Prefix'Length + Path'Length .. Url'Last);

    begin
        return Prefix & Replace_All (Path, "//", "/") & Suffix;
    end Sanitise_Double_Dash;

    function Sanitise_Double_Dots (Url : String) return String is
        Path : constant String := Get_Path (Url);
        Prefix : constant String :=
            Get_Scheme (Url) & "://" & Get_Authority (Url);
        Suffix : constant String :=
            Url (Url'First + Prefix'Length + Path'Length .. Url'Last);

        Upath : Unbounded_String := To_Unbounded_String (Path);

        procedure Replace_Paths;

        procedure Replace_Paths is
            use all type Ada.Strings.Direction;
            I, Back : Natural;

            procedure Next_Double_Dots;
            --  Search the Next "/..".
            --
            --  Avoid things like "/..hello".  Only "/../" and when "/.."
            --  is at the end of the URL.

            procedure Next_Double_Dots is
            begin
                I := Index (Upath, "/..", 2);
                while I > 0 loop
                    exit when I + 3 >= Length (Upath)
                        or else Element (Upath, I + 3) = '/';
                    I := Index (Upath, "/..", I + 2);
                end loop;
            end Next_Double_Dots;
        begin
            Next_Double_Dots;

            while I > 0 loop
                Back := Index (Upath, "/", I - 1, Backward);

                if Back > 0 then
                    --  Pattern "/path/.." found.
                    Replace_Slice (Upath, Back, I + 3, "/");

                    --  No need for else: if not found is an Invalid "/.."
                    --  subpath.  It will be deleted in other procedure.
                end if;

                Next_Double_Dots;
            end loop;
        end Replace_Paths;
    begin

        --  Replace "/path/.." Into "/".
        Replace_Paths;

        --  Remove the rest of "/.." found, because they are Invalid!
        return Prefix & Replace_All (To_String  (Upath), "/..", "/") & Suffix;

    end Sanitise_Double_Dots;

    function To_Local_Path (Url, Root_Path : String;
                            Index_Default : String := "")
        return String
    is
        use Ada.Directories;
        Local_Path : constant String := Root_Path & Get_Path (Sanitise (Url));
    begin
        if Index_Default /= ""
            and then Exists (Local_Path)
            and then Kind (Local_Path) = Directory
            and then Exists (Compose (Local_Path, Index_Default))
        then
            return Compose (Local_Path, Index_Default);
        else
            return Local_Path;
        end if;
    end To_Local_Path;

end Agemini.URLs.Sanitisers;
