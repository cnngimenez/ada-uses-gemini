--  agemini-gmid_infos.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package Agemini.Gmid_Infos is
--  type Cgi_Gmid_Info is record
--      --  Maximum 1024 Bytes
--      Gemini_Url : Unbounded_String;
--      Gemini_Url_Path : Unbounded_String;
--      Path_Info : Unbounded_String;
--      Path_Translated : Unbounded_String;
--      Query_String : Unbounded_String;
--      Remote_Addr : Unbounded_String;
--      Remote_Host : Unbounded_String;
--      Request_Method : Unbounded_String;
--      Server_Name : Unbounded_String;
--      Server_Protocol : Unbounded_String;
--      Auth_Type : Gemini_Authentication_Type;
--      Remote_User : Unbounded_String;
--      Tls_Client_Issuer : Unbounded_String;
--      Tls_Client_Hash : Unbounded_String;
--      Tls_Version : Unbounded_String;
--      Tls_Cipher : Unbounded_String;
--      Tls_Cipher_Strength : Unbounded_String;
--      Tls_Client_Not_After : Unbounded_String;
--      Tls_Client_Not_Before : Unbounded_String;
--  end record;
end Agemini.Gmid_Infos;
