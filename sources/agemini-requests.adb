--  agemini-requests.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Calendar.Formatting;
with Agemini.URLs.Sanitisers;

package body Agemini.Requests is

    function Get_Host (Self : Request_Type) return String
        is (To_String (Self.Host));

    function Get_Port (Self : Request_Type) return TLS.Contexts.Port_Number
        is (Self.Port);

    function Get_URL (Self : Request_Type) return String
        is (To_String (Self.URL));

    function Get_TLS_Connection (Self : Request_Type)
        return TLS.Contexts.Connection_Info
        is (Self.TLS_Connection);

    function Get_TLS_Certificate (Self : Request_Type)
        return TLS.Contexts.Certificate_Info
        is (Self.TLS_Certificate);

    function Image (Self : Request_Type) return String is
        function Connection_Image return String;
        function Certificate_Image return String;

        function Certificate_Image return String is
        begin
            if not Self.Peer_Certificate_Provided then
                return "TLS Certificate: Not provided!";
            end if;
            return "  TLS_Certificate:" & ASCII.LF
            & "    Hash: """
            & To_String (Self.TLS_Certificate.Hash) & """" & ASCII.LF
            & "    Issuer: """
            & To_String (Self.TLS_Certificate.Issuer) & """" & ASCII.LF
            & "    Subject: """
            & To_String (Self.TLS_Certificate.Subject) & """" & ASCII.LF
            & "    Not Before: "
            & Ada.Calendar.Formatting.Image
                (Self.TLS_Certificate.Not_Before, True)
            & ASCII.LF
            & "    Not After: "
            & Ada.Calendar.Formatting.Image
                (Self.TLS_Certificate.Not_After, True)
            & ASCII.LF;
        end Certificate_Image;

        function Connection_Image return String is
        begin
            return "  Connection Info: " & ASCII.LF
            & "    TLS Version: """
            & To_String (Self.TLS_Connection.TLS_Version) & """" & ASCII.LF
            & "    Cipher: """
            & To_String (Self.TLS_Connection.Cipher) & """" & ASCII.LF
            & "    Cipher Strength: "
            & Self.TLS_Connection.Cipher_Strength'Image & ASCII.LF
            & "    ALPN: """
            & To_String (Self.TLS_Connection.ALPN) & """" & ASCII.LF;
        end Connection_Image;

    begin
        return "Request:" & ASCII.LF
            & "  URL: """ & To_String (Self.URL) & """" & ASCII.LF
            & "  Client Host: """ & To_String (Self.Host) & """" & ASCII.LF
            & "  Client Port: " & Self.Port'Image & ASCII.LF
            --  & Connection_Image
            & "  Peer Certificate Provided: "
            & Self.Peer_Certificate_Provided'Image & ASCII.LF
            & Certificate_Image;
    end Image;

    procedure Initialise (Self : in out Request_Type;
                          Url : String)
    is
        Sanitised_Url : constant String :=
            Agemini.URLs.Sanitisers.Sanitise (Url);
    begin
        Self.Peer_Certificate_Provided := False;
        Self.URL := To_Unbounded_String (Sanitised_Url);
    end Initialise;

    procedure Initialise (Self : in out Request_Type;
                          Url : Unbounded_String)
    is
        Sanitised_Url : constant String :=
            Agemini.URLs.Sanitisers.Sanitise (To_String (Url));
    begin
        Self.Peer_Certificate_Provided := False;
        Self.URL := To_Unbounded_String (Sanitised_Url);
    end Initialise;

    function Is_Peer_Certificate_Provided (Self : Request_Type) return Boolean
        is (Self.Peer_Certificate_Provided);

    procedure Set_Host (Self : in out Request_Type; Host : String)
    is
    begin
        Self.Host := To_Unbounded_String (Host);
    end Set_Host;

    procedure Set_Port (Self : in out Request_Type;
                        Port : TLS.Contexts.Port_Number)
    is
    begin
        Self.Port := Port;
    end Set_Port;

    procedure Set_TLS_Certificate (Self : in out Request_Type;
                                   Certificate : TLS.Contexts.Certificate_Info)
    is
    begin
        Self.Peer_Certificate_Provided := True;
        Self.TLS_Certificate := Certificate;
    end Set_TLS_Certificate;

    procedure Set_TLS_Connection (Self : in out Request_Type;
                                  Info : TLS.Contexts.Connection_Info)
    is
    begin
        Self.TLS_Connection := Info;
    end Set_TLS_Connection;

end Agemini.Requests;
