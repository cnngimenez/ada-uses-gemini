--  agemini-responses.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Strings;
with Ada.Strings.Fixed;
with Ada.Characters.Latin_1;
with Ada.Characters;

package body Agemini.Responses is

    function Get_Body (Self : Response_Type) return Unbounded_String is
    begin
        case Self.Body_Content is
        when Text_Data =>
            return Self.Body_Text;
        when File_Data =>
            return Self.Filepath;
        end case;
    end Get_Body;

    function Get_Body (Self : Response_Type) return String is
        (To_String (Get_Body (Self)));

    function Get_Meta (Self : Response_Type) return Unbounded_String is
        (Self.Meta);

    function Get_Meta (Self : Response_Type) return String is
        (To_String (Self.Meta));

    function Get_Status (Self : Response_Type) return Status_Type is
        (Self.Status);

    procedure Initialise (Self : in out Response_Type;
                          Status : Status_Type := Success;
                          Meta : String := "text/gemini; charset=utf-8";
                          Body_Text : String := "";
                          Body_Content : Body_Data_Type := Text_Data) is
        Temp : Response_Type (Body_Content);
    begin
        Temp.Status := Status;
        Temp.Meta := To_Unbounded_String (Meta);
        --  Temp.Body_Content := Body_Content;
        case Temp.Body_Content is
        when Text_Data =>
            Temp.Body_Text := To_Unbounded_String (Body_Text);
        when File_Data =>
            Temp.Filepath := To_Unbounded_String (Body_Text);
        end case;
        Self := Temp;
    end Initialise;

    procedure Send (Self : Response_Type) is
        use Ada.Text_IO;
    begin
        Put (To_String (Self));
    end Send;

    procedure Set_Body (Self : in out Response_Type; Body_Text : String) is
    begin
        case Self.Body_Content is
        when Text_Data =>
            Self.Body_Text := To_Unbounded_String (Body_Text);
        when File_Data =>
            Self.Filepath := To_Unbounded_String (Body_Text);
        end case;
    end Set_Body;

    procedure Set_Meta (Self : in out Response_Type; Meta : String) is
    begin
        Self.Meta := To_Unbounded_String (Meta);
    end Set_Meta;

    function To_String (Self : Response_Type) return String is
        use Ada.Strings;
        use Ada.Characters;

        Status_String : constant String :=
            Fixed.Trim (Self.Status'Enum_Rep'Image, Both);
    begin
        --  <status> <meta> <crlf>
        case Self.Body_Content is
        when Text_Data =>
            return Status_String & ' ' & To_String (Self.Meta)
                   & Latin_1.CR & Latin_1.LF
                   & To_String (Self.Body_Text);
        when File_Data =>
            return Status_String & ' ' & To_String (Self.Meta)
                   & Latin_1.CR & Latin_1.LF;
        end case;
    end To_String;

end Agemini.Responses;
