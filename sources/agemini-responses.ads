--  agemini-responses.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------
with Ada.Strings.Unbounded;
with Agemini.Statuses;

package Agemini.Responses is
    use Ada.Strings.Unbounded;
    use Agemini.Statuses;

    type Body_Data_Type is (Text_Data, File_Data);
    --  The content is of type text or a file path?

    type Response_Type (Body_Content : Body_Data_Type := Text_Data) is private;

    --  The type to represent Responses.
    --
    --  The body contents can be a provided from a text string (Text_Data) or
    --  from a file (File_Data). By default setted as a Text_Data.
    --
    --  Extracting data from a file is useful for sending a big file by
    --  reading chunks. If Text_Data is used in this cases, the file has to be
    --  loaded completely on RAM before sending.

    procedure Initialise (Self : in out Response_Type;
                          Status : Status_Type := Success;
                          Meta : String := "text/gemini; charset=utf-8";
                          Body_Text : String := "";
                          Body_Content : Body_Data_Type := Text_Data);
    --  Initialise the Response Object.
    --
    --  If Body_Content is File_Data, then Body_Text must be the filepath To
    --  where extract the data.

    procedure Send (Self : Response_Type);
    --  Send response to the CGI server (and then to the client, supposedly).

    procedure Set_Body (Self : in out Response_Type; Body_Text : String);
    procedure Set_Meta (Self : in out Response_Type; Meta : String);

    function Get_Status (Self : Response_Type) return Status_Type;
    function Get_Meta (Self : Response_Type) return Unbounded_String;
    function Get_Meta (Self : Response_Type) return String;
    function Get_Body (Self : Response_Type) return Unbounded_String;
    function Get_Body (Self : Response_Type) return String;

    function To_String (Self : Response_Type) return String;

private

    type Response_Type (Body_Content : Body_Data_Type := Text_Data) is record
        Status : Status_Type := Agemini.Statuses.Success;
        --  Status should be a two digit number only.
        Meta : Unbounded_String := Null_Unbounded_String;
        --  Meta Should not exceed 1024.

        case Body_Content is
        when Text_Data =>
            Body_Text : Unbounded_String := Null_Unbounded_String;
        when File_Data =>
            Filepath : Unbounded_String := Null_Unbounded_String;
        end case;

    end record;

end Agemini.Responses;
