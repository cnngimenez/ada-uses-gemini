--  agemini-requests.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with TLS.Contexts;

package Agemini.Requests is

    type Request_Type is tagged private;

    procedure Initialise (Self : in out Request_Type;
                          Url : String);
    procedure Initialise (Self : in out Request_Type;
                          Url : Unbounded_String);

    function Get_URL (Self : Request_Type) return String;

    --  Client extra information.  These is not in the Gemini specification.

    function Get_TLS_Connection (Self : Request_Type)
        return TLS.Contexts.Connection_Info;
    function Get_TLS_Certificate (Self : Request_Type)
        return TLS.Contexts.Certificate_Info;
    function Get_Host (Self : Request_Type) return String;
    function Get_Port (Self : Request_Type) return TLS.Contexts.Port_Number;
    function Is_Peer_Certificate_Provided (Self : Request_Type) return Boolean;

    procedure Set_TLS_Connection (Self : in out Request_Type;
                                  Info : TLS.Contexts.Connection_Info);
    procedure Set_TLS_Certificate
        (Self : in out Request_Type;
         Certificate : TLS.Contexts.Certificate_Info);
    procedure Set_Host (Self : in out Request_Type; Host : String);
    procedure Set_Port (Self : in out Request_Type;
                        Port : TLS.Contexts.Port_Number);

    function Image (Self : Request_Type) return String;

private

    type Request_Type is tagged record
        URL : Unbounded_String;

        --  These fields are Client extra info.
        Host : Unbounded_String;
        Port : TLS.Contexts.Port_Number;
        TLS_Connection : TLS.Contexts.Connection_Info;
        Peer_Certificate_Provided : Boolean;
        TLS_Certificate : TLS.Contexts.Certificate_Info;
    end record;

end Agemini.Requests;
