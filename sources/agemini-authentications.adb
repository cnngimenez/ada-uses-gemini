--  agemini-authentications.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;

package body Agemini.Authentications is
    function Load_From_File (Path : String) return Authentication_Vector is

        function Read_Auth (Hash_File : File_Type) return Authentication;

        function Read_Auth (Hash_File : File_Type) return Authentication is
            use Hash_Strings;
            Auth : Authentication;
        begin
            Auth.Common_Name := To_Unbounded_String (Get_Line (Hash_File));

            if not End_Of_Line (Hash_File) then
                Auth.Hash := To_Bounded_String (Get_Line (Hash_File));
            else
                Auth.Hash := To_Bounded_String ("");
            end if;

            return Auth;
        end Read_Auth;

        Hash_File : File_Type;
        Auth : Authentication;
        Hashes : Authentication_Vector;
    begin
        Open (Hash_File, In_File, Path);

        while not End_Of_File (Hash_File) loop
            Auth := Read_Auth (Hash_File);
            Auth_Vector_Package.Append (Hashes, Auth);
        end loop;

        Close (Hash_File);

        return Hashes;
    end Load_From_File;

    function Member (Hashes : Authentication_Vector; Auth : Authentication)
                    return Boolean is
    begin
        return Auth_Vector_Package.Contains (Hashes, Auth);
    end Member;

    function Member (Hashes : Authentication_Vector; Hash : String)
                    return Boolean is
        use Hash_Strings;
        Found : Boolean := False;
        Current : Auth_Vector_Package.Cursor :=
          Auth_Vector_Package.First (Hashes);
    begin
        while not Found
          and then Auth_Vector_Package.Has_Element (Current)
        loop
            Found := Auth_Vector_Package.Element (Current).Hash = Hash;
            Current := Auth_Vector_Package.Next (Current);
        end loop;

        return Found;
    end Member;

end Agemini.Authentications;
