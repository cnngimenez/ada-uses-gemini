--  agemini-statuses.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package Agemini.Statuses is
    type Status_Number is range 0 .. 99;
    --  Status number codes must be a two digit number.

    type Status_Type is (Input,
                         Sensitive_Input,
                         Success,
                         Redirect_Temporary,
                         Redirect_Permanent,
                         Temporary_Failure,
                         Server_Unavailable,
                         Cgi_Error,
                         Proxy_Error,
                         Slow_Down,
                         Permanent_Failure,
                         Not_Found,
                         Gone,
                         Proxy_Request_Refused,
                         Bad_Request,
                         Client_Certificate_Required,
                         Certificate_Not_Authorised,
                         Certificate_Not_Valid);
    for Status_Type use (Input => 10,
                         Sensitive_Input => 11,
                         Success => 20,
                         Redirect_Temporary => 30,
                         Redirect_Permanent => 31,
                         Temporary_Failure => 40,
                         Server_Unavailable => 41,
                         Cgi_Error => 42,
                         Proxy_Error => 43,
                         Slow_Down => 44,
                         Permanent_Failure => 50,
                         Not_Found => 51,
                         Gone => 52,
                         Proxy_Request_Refused => 53,
                         Bad_Request => 59,
                         Client_Certificate_Required => 60,
                         Certificate_Not_Authorised => 61,
                         Certificate_Not_Valid => 62);
end Agemini.Statuses;
