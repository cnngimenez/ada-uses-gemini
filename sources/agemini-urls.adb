--  agemini-urls.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Fixed;
use Ada.Strings.Fixed;

package body Agemini.URLs is

    function Get_Authority (Url : String) return String is
        Start, Ending : Natural;
    begin
        Start := Index (Url, "://");
        if Start > 0 then
            Start := Start + 3; --  Skip "://" characters
        end if;

        Ending := Index (Url, "/", Start);
        if Ending > 0 then
            --  Ignore "/"...
            --  Case: "gemini://myhostname.org/some/path/here.gmi?query=1"
            Ending := Ending - 1;
            return Url (Start .. Ending);
        end if;

        --  "/" not found... search for "?"
        Ending := Index (Url, "?", Start);
        if Ending > 0 then
            --  Case: "gemini://myhostname.org?query=1"
            Ending := Ending - 1;
        else
            --  also not found.. use the end of string.
            --  Case: "gemini://myhostname.org"
            Ending := Url'Last;
        end if;

        return Url (Start .. Ending);
    end Get_Authority;

    function Get_File (Url : String) return String is
        Path : constant String := Get_Path (Url);
        File_Start : Natural;
    begin
        File_Start := Index (Path, "/", Path'Last, Ada.Strings.Backward);
        if File_Start > 0 then
            --  Ignore starting "/".
            File_Start := File_Start + 1;
        end if;

        return Path (File_Start .. Path'Last);
    end Get_File;

    function Get_Host (Url : String) return String is
        Auth : constant String := Get_Authority (Url);
        Start, Ending : Natural;
    begin
        Start := Index (Auth, "@", Auth'First);
        if Start = 0 then
            Start := Auth'First;
        else
            Start := Start + 1;
        end if;

        Ending := Index (Auth, ":", Start);
        if Ending = 0 then
            Ending := Auth'Last;
        else
            Ending := Ending - 1;
        end if;

        return Auth (Start .. Ending);
    end Get_Host;

    function Get_Key (Query : Unbounded_String) return String is
        Pos : Natural;
    begin
        Pos := Index (Query, "=");
        if Pos = 0 then
            return "";
        else
            return Slice (Query, 1, Pos - 1);
        end if;
    end Get_Key;

    function Get_Path (Url : String) return String is
        procedure Skip_Hostname;
        procedure Skip_Protocol;

        Start, Ending : Natural;

        procedure Skip_Hostname is
        begin
            Start := Index (Url, "/", Start);
            --  No! We do not want to skip the initial "/"!
            --  if Start > 0 then
            --      --  Skip "/" too.
            --      Start := Start + 1;
            --  end if;
        end Skip_Hostname;

        procedure Skip_Protocol is
        begin
            Start := Index (Url, "://");
            if Start > 0 then
                --  Skip "://" too.
                Start := Start + 3;
            end if;
        end Skip_Protocol;
    begin
        Skip_Protocol;
        Skip_Hostname;

        Ending := Index (Url, "?", Start);
        if Ending = 0 then
            --  "?" not found... then use the end of the string.
            Ending := Url'Last;
        else
            --  Do not include "?".
            Ending := Ending - 1;
        end if;

        return Url (Start .. Ending);
    end Get_Path;

    function Get_Port (Url : String) return String is
        Auth : constant String := Get_Authority (Url);
        Start : Natural;
    begin
        --  Search for the end of the username.
        Start := Index (Auth, "@", Auth'First);
        if Start = 0 then
            Start := Auth'First;
        end if;

        --  Go to the end of the host.
        Start := Index (Auth, ":", Start);
        if Start = 0 then
            return "";
        end if;

        return Auth (Start + 1 .. Auth'Last);
    end Get_Port;

    function Get_Port (Url : String) return GNAT.Sockets.Port_Type is
        Port_String : constant String := Get_Port (Url);
    begin
        if Port_String = "" then
            return 0;
        else
            return GNAT.Sockets.Port_Type'Value (Port_String);
        end if;
    end Get_Port;

    function Get_Protocol (Url : String) return String is
        Pos : Natural;
    begin
        Pos := Index (Url, "://");
        if Pos = 0 then
            return "";
        else
            return Url (Url'First .. Pos - 1);
        end if;
    end Get_Protocol;

    function Get_Queries (Url : String) return String is
        Pos : Natural;
    begin
        Pos := Index (Url, "?");
        if Pos = 0 then
            return "";
        else
            return Url (Pos + 1 .. Url'Last);
        end if;
    end Get_Queries;

    function Get_Query_Value (Url : String;
                              Key : String) return String is
        use String_Vector_Package;
        function Has_The_Key (C : Cursor) return Boolean;

        function Has_The_Key (C : Cursor) return Boolean is
        begin
            return Get_Key (Element (C)) = Key;
        end Has_The_Key;

        Queries : constant String_Vector := Split (Url, '&');
        Query_Cursor : Cursor := Queries.First;
    begin
        while Has_Element (Query_Cursor)
          and then not Has_The_Key (Query_Cursor)
        loop
            Next (Query_Cursor);
        end loop;

        if Has_Element (Query_Cursor) then
            return Get_Value (Element (Query_Cursor));
        else
            return "";
        end if;
    end Get_Query_Value;

    function Get_Userinfo (Url : String) return String is
        Auth : constant String := Get_Authority (Url);
        Ending : Natural;
    begin
        Ending := Index (Auth, "@", Auth'First);
        if Ending = 0 then
            return "";
        end if;

        return Auth (Auth'First .. Ending - 1);
    end Get_Userinfo;

    function Get_Value (Query : Unbounded_String) return String is
        Pos : Natural;
    begin
        Pos := Index (Query, "=");
        if Pos = 0 then
            return To_String (Query);
        else
            return Slice (Query, Pos + 1, Length (Query));
        end if;
    end Get_Value;

    function Parse_Queries (Query_String : String) return Query_Map is
        use String_Vector_Package;
        procedure Add_Map (C : String_Vector_Package.Cursor);

        Queries : constant String_Vector := Split (Query_String, '&');
        Results : Query_Map;

        procedure Add_Map (C : String_Vector_Package.Cursor) is
            Key, Value : Unbounded_String;
        begin
            Query_Key_Value (Element (C), Key, Value);
            Query_Hash_Package.Insert (Results, Key, Value);
        end Add_Map;
    begin
        Queries.Iterate (Add_Map'Access);
        return Results;
    end Parse_Queries;

    procedure Query_Key_Value (Query : Unbounded_String;
                               Key : out Unbounded_String;
                               Value : out Unbounded_String) is
        Pos : Natural;
    begin
        Pos := Index (Query, "=");
        if Pos = 0 then
            Key := Query;
            Value := Query;
        else
            Key := Unbounded_Slice (Query, 1, Pos - 1);
            Value := Unbounded_Slice (Query, Pos + 1, Length (Query));
        end if;
    end Query_Key_Value;

    function Split (Str : String; Separator : Character) return String_Vector
    is
        use String_Vector_Package;

        Results : String_Vector;
        Current : Unbounded_String := Null_Unbounded_String;
    begin
        for I in Str'Range loop
            if Str (I) = Separator then
                Append (Results, Current);
                Current := Null_Unbounded_String;
            else
                Append (Current, Str (I));
            end if;
        end loop;

        if Current /= Null_Unbounded_String then
            Append (Results, Current);
        end if;

        return Results;
    end Split;
end Agemini.URLs;
