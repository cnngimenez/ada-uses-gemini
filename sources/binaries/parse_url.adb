--  parse_url.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Directories;
with Agemini.URLs;
use Agemini.URLs;
with Agemini.URLs.Sanitisers;

procedure Parse_Url is
    procedure Parse_Arguments (Url : String);
    procedure Parse_Key (Url : String; Key : String);
    procedure Print_Url_Parts (Url : String);
    procedure Print_Local_Path (Url : String);
    procedure Print_Validations (Url : String);

    procedure Parse_Arguments (Url : String) is
        use Query_Hash_Package;
        procedure Show_Mapping (C : Cursor);

        procedure Show_Mapping (C : Cursor) is
        begin
            if Has_Element (C) then
                Put_Line ("   """
                            & To_String (Key (C))
                            & """ := """
                            & To_String (Element (C))
                            & """");
            else
                Put_Line ("-");
            end if;
        end Show_Mapping;

        Map : Query_Map;
    begin
        Put_Line ("** Parsing arguments:");

        Map := Parse_Queries (Url);
        Iterate (Map, Show_Mapping'Access);
    end Parse_Arguments;

    procedure Parse_Key (Url : String; Key : String) is
    begin
        Put_Line ("* User Query");
        Put_Line ("  Value of """
            & Key & """: """
            & Get_Query_Value (Url, Key) & """");
    end Parse_Key;

    procedure Print_Local_Path (Url : String) is
        Local_Path : constant String :=
            Agemini.URLs.Sanitisers.To_Local_Path (Url, ".");
        Local_Path_Fixed : constant String :=
            Agemini.URLs.Sanitisers.To_Local_Path (Url, ".", "index.gmi");
        Exists_Fixed : constant Boolean :=
            Ada.Directories.Exists (Local_Path_Fixed);
        Exists : constant Boolean :=
            Ada.Directories.Exists (Local_Path);
    begin
        Put_Line ("* URL to local path");
        Put_Line ("  URL path: " & Get_Path (Url));
        Put_Line ("  Local path: " & Local_Path);
        Put_Line ("  Exists? "
            & Boolean'Image (Exists));
        if Exists then
            Put_Line ("  File kind? "
                & Ada.Directories.File_Kind'Image
                    (Ada.Directories.Kind (Local_Path)));
        end if;

        Put_Line ("** Fixed");
        Put_Line ("   Local path fixed: " & Local_Path_Fixed);
        Put_Line ("   Exists? "
            & Boolean'Image (Exists_Fixed));
        if Exists_Fixed then
            Put_Line ("   File kind? "
                & Ada.Directories.File_Kind'Image
                    (Ada.Directories.Kind (Local_Path_Fixed)));
        end if;
    end Print_Local_Path;

    procedure Print_Url_Parts (Url : String) is
    begin
        Put_Line ("* URL Parts");
        Put_Line ("  Scheme: """ & Get_Scheme (Url) & """");
        Put_Line ("  Authority: """ & Get_Authority (Url) & """");
        Put_Line ("      Userinfo: """ & Get_Userinfo (Url) & """");
        Put_Line ("      Host: """ & Get_Host (Url) & """");
        Put_Line ("      Port: """ & Get_Port (Url) & """");
        Put_Line ("  Path: """ & Get_Path (Url) & """");
        Put_Line ("  File: """ & Get_File (Url) & """");
        Put_Line ("  Queries: """ & Get_Queries (Url) & """");
        --  Put_Line ("Fragment: """ & Get_Fragment (Url) & """");
    end Print_Url_Parts;

    procedure Print_Validations (Url : String) is
        use Agemini.URLs.Sanitisers;

        Valid : constant Boolean := Is_Syntax_Valid (Url);
    begin
        Put_Line ("* Validation");
        Put_Line ("  Validations are checked against RFC 3986.");
        Put_Line ("  URL original   : " & Url);
        Put_Line ("  Valid syntax   : " & Valid'Image);
        Put_Line ("  Valid Scheme   : "
            & Boolean'Image (Is_Scheme_Valid (Get_Scheme (Url))));
        Put_Line ("  Valid Authority: "
            & Boolean'Image (Is_Authority_Valid (Get_Authority (Url))));
        Put_Line ("  Valid Userinfo : "
            & Boolean'Image (Is_Scheme_Valid (Get_Scheme (Url))));
        Put_Line ("  Valid Host     : "
            & Boolean'Image (Is_Authority_Valid (Get_Authority (Url))));
        Put_Line ("  Valid Port     : "
            & Boolean'Image (Is_Scheme_Valid (Get_Scheme (Url))));
        Put_Line ("  Valid Query    : "
            & Boolean'Image (Is_Authority_Valid (Get_Authority (Url))));
        Put_Line ("  Valid Fragment : "
            & Boolean'Image (Is_Scheme_Valid (Get_Scheme (Url))));

        Put_Line ("* Sanitisation");
        Put_Line ("  URL original                   : " & Url);
        Put_Line ("  URL sanitised                  : "
            & Sanitise (Url));
        Put_Line ("  URL sanitise dots (""."")        : "
            & Sanitise_Dots (Url));
        Put_Line ("  URL Sanitise double dots (""..""): "
            & Sanitise_Double_Dots (Url));
        Put_Line ("  URL sanitise double Dash (""//""): "
            & Sanitise_Double_Dash (Url));
    end Print_Validations;
begin
    if Argument_Count = 0 then
        Put_Line ("Synopsis:");
        Put_Line ("    parse_url URL [QUERY_KEY]");
        return;
    end if;

    Print_Url_Parts (Argument (1));
    Parse_Arguments (Get_Queries (Argument (1)));
    Print_Local_Path (Argument (1));
    Print_Validations (Argument (1));

    if Argument_Count >= 2 then
        Parse_Key (Get_Queries (Argument (1)), Argument (2));
    end if;

end Parse_Url;
