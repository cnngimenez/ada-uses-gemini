--  check_tls.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

--  Check for a specific TLS certificate.
--  If the user does not provide a certificate: ask for it.
--  If it is not the correct certificate hash, do not authorise.
with Ada.Directories;
use Ada.Directories;
with Agemini.Dezhemini_Infos;
use Agemini.Dezhemini_Infos;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Statuses;
use Agemini.Statuses;
with Agemini.Authentications;
use Agemini.Authentications;

procedure Check_Tls is
    procedure Do_Authentication;

    Accepted_Hashes_Path : constant String :=
      "hashes.txt";
    Parameters : constant Cgi_Dezhemini_Info := Initialise_From_System;
    Response : Response_Type;

    procedure Do_Authentication is
        Accepted_Auths : constant Authentication_Vector :=
          Load_From_File (Accepted_Hashes_Path);
    begin
        if Parameters.Is_Authenticated (Accepted_Auths) then
            Response.Initialise (Status => Agemini.Statuses.Success,
                                 Body_Text => "# Welcome!");
            Response.Send;
        else
            Response.Initialise (Status => Certificate_Not_Authorised,
                                 Meta => "Not authorised!");
            Response.Send;
        end if;
    end Do_Authentication;

begin
    if not Ada.Directories.Exists (Accepted_Hashes_Path) then
        Response.Initialise
          (Status => Agemini.Statuses.Cgi_Error,
           Meta => "The hashes file does not exists.");
        Response.Send;
        return;
    end if;

    if not (Ada.Directories.Kind (Accepted_Hashes_Path) = Ordinary_File) then
        Response.Initialise
          (Status => Agemini.Statuses.Cgi_Error,
           Meta => "The hashes path does not point to a regular file.");
        Response.Send;
        return;
    end if;

    if Parameters.Is_Authenticated  then
        Do_Authentication;
    else
        Response.Initialise
          (Status => Agemini.Statuses.Client_Certificate_Required,
           Meta => "Please, provide a user certificate.");
        Response.Send;
    end if;
end Check_Tls;
