--  check_input.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

--
--  Ask for an input and check if it is a particullar text.
--
--  If it is, return success, if not ask again.
--

with Agemini.Dezhemini_Infos;
use Agemini.Dezhemini_Infos;
with Agemini.Responses;
use Agemini.Responses;
with Agemini.Statuses;
use Agemini.Statuses;

procedure Check_Input is
    Parameters : constant Cgi_Dezhemini_Info := Initialise_From_System;
    Response : Response_Type;
begin
    if Parameters.Get_Parameter (Query_String) = "" then
        Response.Initialise (Status => Input,
                             Meta => "Please, enter your name.");
    else
        Response.Initialise (Status => Success,
                             Body_Text => "# Hello "
                               &  Parameters.Get_Parameter (Query_String)
                               & "!");
    end if;

    Response.Send;

end Check_Input;
