--  agemini-dezhemini_infos.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Agemini.Authentications;
use Agemini.Authentications;

package Agemini.Dezhemini_Infos is
    type Cgi_Dezhemini_Info (Auth_Type : Authentication_Type) is
      tagged private;

    type Parameter_Name is (Document_Root, Gateway_Interface, Gemini_Url,
                            Path_Info, Path_Translated, Query_String,
                            Script_Name, Server_Name, Server_Protocol,
                            Auth_Type, Remote_User, Tls_Client_Hash);

    function Initialise_From_System return Cgi_Dezhemini_Info;
    --  From the environment variables, initialise the record.

    function Get_Parameter (Self : Cgi_Dezhemini_Info;
                            Name : Parameter_Name) return Unbounded_String;
    function Get_Parameter (Self : Cgi_Dezhemini_Info;
                            Name : Parameter_Name) return String;

    function Is_Authenticated (Self : Cgi_Dezhemini_Info) return Boolean;
    --  Has the parameters provide client authentication?

    function Is_Authenticated (Self : Cgi_Dezhemini_Info;
                               Certificate_Hash : String) return Boolean;
    function Is_Authenticated (Self : Cgi_Dezhemini_Info;
                               Accepted_Auths : Authentication_Vector)
                              return Boolean;

    function To_String (Self : Cgi_Dezhemini_Info) return String;
    --  A fast way to show all parameter data.

private
    type Cgi_Dezhemini_Info (Auth_Type : Authentication_Type) is tagged record
        Document_Root : Unbounded_String;
        Gateway_Interface : Unbounded_String;
        Gemini_Url : Unbounded_String;
        Path_Info : Unbounded_String;
        Path_Translated : Unbounded_String;
        Query_String : Unbounded_String;
        Script_Name : Unbounded_String;
        Server_Name : Unbounded_String;
        Server_Protocol : Unbounded_String;
        Server_Software : Unbounded_String;
        case Auth_Type is
           when Certificate =>
               Remote_User : Unbounded_String;
               Tls_Client_Hash : Unbounded_String;
           when None =>
               null;
        end case;
    end record;

    procedure Initialise_Common_Info (Self : in out Cgi_Dezhemini_Info);

end Agemini.Dezhemini_Infos;
