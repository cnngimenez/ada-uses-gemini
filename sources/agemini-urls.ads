--  agemini-urls.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Containers.Hashed_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Hash;
with GNAT.Sockets;

--  Parsing URL.
--
--  This is a simple library to parse URL and retrieve its parts. Also, the
--  URL queries can be parsed one by one or Altogether.
--
--  See RFC 3986 for more information about URL syntax.
package Agemini.URLs is

    function Get_Protocol (Url : String) return String;
    --  Return the scheme used in the URL.
    --
    --  For instance, "gemini://hostname.org" return "gemini".

    function Get_Scheme (Url : String) return String renames Get_Protocol;

    function Get_Host (Url : String) return String;
    --  Get the host name in the authority part of the URL.
    --
    --  For instance:
    --  "gemini://hostname.org/path" return "hostname.org"
    --  "hostname.org/path" return "hostname.org"
    --  "hostname.org?query=1" return "hostname.Org"

    function Get_Userinfo (Url : String) return String;

    function Get_Port (Url : String) return String;
    function Get_Port (Url : String) return GNAT.Sockets.Port_Type;

    function Get_Authority (Url : String) return String;
    --  Return the authority part in the URL.
    --
    --  The authority syntax is:

    function Get_Path (Url : String) return String;
    --  Get the path after the hostname in URL.
    --
    --  For instance:
    --  "hostname.org/a/path/here.gmi" return "/a/path/here.gmi"
    --  "hostname.org/a/path/heer.gmi?query=1" return "/a/path/here.gmi"

    function Get_File (Url : String) return String;
    --  Get the file name if explicit in the URL.
    --
    --  Examples:
    --  "hostname.org/a/path/here.gmi" return "here.gmi"
    --  "hostname.org/a/path/here" return "here"
    --  "hostname.org/a/path/" return ""

    function Get_Queries (Url : String) return String;
    --  Return the queries in the URL.
    --
    --  Examples:
    --  "hostname.org/path" return ""
    --  "hostname.org/path?query=1&question=2" return "query=1&question=2"

    package Query_Hash_Package is new Ada.Containers.Hashed_Maps
      (Key_Type => Unbounded_String,
       Element_Type => Unbounded_String,
       Hash => Ada.Strings.Unbounded.Hash,
       Equivalent_Keys => "=");

    subtype Query_Map is Query_Hash_Package.Map;
    --  A map to store all URL queries.
    --
    --  Store all "key=value" query parsed from the URL into a map variable.
    --  This can be used when parsing several queries from the URL. For only
    --  one query, the Get_Query_Value function looks directly for that name,
    --  but it is inefficient for searching several values.

    function Parse_Queries (Query_String : String) return Query_Map;
    --  Parse all queries and return a hash with Key-Value.
    --
    --  Return a map with all the queries found on the Query_String. Use the
    --  Get_Queries function to retrieve the parameter.
    --
    --  Example:
    --  The_Map : Query_Map;
    --  The_Map := Parse_Queries (Get_Queries (Url));

    function Get_Query_Value (Url : String;
                              Key : String) return String;
    --  Search for a value in the URL query.
    --
    --  Search the query in the URL, and find the 'key=value'. Return the
    --  value.
    --
    --  If you require to find several keys, then use: Parse_Queries (Url).

private
    package String_Vector_Package is new Ada.Containers.Vectors
      (Index_Type => Positive,
       Element_Type => Unbounded_String);
    subtype String_Vector is String_Vector_Package.Vector;

    function Split (Str : String; Separator : Character) return String_Vector;
    --  Split the string into several Unbounded_String using a character.

    function Get_Key (Query : Unbounded_String) return String;
    function Get_Value (Query : Unbounded_String) return String;
    procedure Query_Key_Value (Query : Unbounded_String;
                               Key : out Unbounded_String;
                               Value : out Unbounded_String);

end Agemini.URLs;
