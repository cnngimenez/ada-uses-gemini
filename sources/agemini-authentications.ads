--  agemini-authentications.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Bounded;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;

package Agemini.Authentications is
    package Hash_Strings is new Ada.Strings.Bounded.Generic_Bounded_Length
      (Max => 135);
    --  135 is the hash string for SHA512 with the "SHA512:" string appended.
    --  However, SHA256 has 71 characters (with "SHA256:" string appended).

    subtype Hash_Type is Hash_Strings.Bounded_String;

    type Authentication is record
        Common_Name : Unbounded_String;
        Hash : Hash_Type;
    end record;

    package Auth_Vector_Package is new Ada.Containers.Vectors
      (Element_Type => Authentication,
       Index_Type => Positive);

    subtype Authentication_Vector is Auth_Vector_Package.Vector;

    function Load_From_File (Path : String) return Authentication_Vector;

    function Member (Hashes : Authentication_Vector; Auth : Authentication)
                    return Boolean;
    function Member (Hashes : Authentication_Vector; Hash : String)
                    return Boolean;
end Agemini.Authentications;
